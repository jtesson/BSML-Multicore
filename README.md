# Shared memory BSML 

BSML is a parallel extension to Ocaml that bring parallelism through
explicit parallel primitives following the Bulk Synchronous
Parallelism model.

# Organisation
*  src contains the implementation of BSML based on Cambridge's
   multicore Ocaml
*  model contains a model of the semantics of BSML and a proof, in a
   simplified settings, that the implementation satisfies this
   semantics
*  Explantory papers are in the paper directory
*  References and bibtex are in the biblio  directory
   
# Installation 
Needs ocaml multicore :
* ``opam remote add multicore https://github.com/ocamllabs/multicore-opam.git ``
* ``opam switch 4.02.2+multicore ;  eval `opam config env` `` 
* ``opam install camlp4``
