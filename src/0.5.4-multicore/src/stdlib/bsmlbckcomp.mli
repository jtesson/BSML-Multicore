module Make:functor (Bsml : Bsmlsig.BSML) ->
sig
  (** For backward compatibility *)

  (** See the documentation of version 0.1. Those functions must be
      avoided from now on. *)

  val bsml_begin : unit -> unit
  val bsml_end : unit -> unit

  exception Get_failure of string
  val get :
    'a Bsml.par -> int list Bsml.par -> (int, 'a) Hashtbl.t Bsml.par

  exception Put_failure of string
  val put : (int * 'a) list Bsml.par -> (int, 'a) Hashtbl.t Bsml.par

  val bsml_abort_string : string -> unit

  (* Bsmlcomm *)

  val scatter :
    ('a -> (int * 'b) list) -> int -> 'a Bsml.par -> 'b Bsml.par

  (** See the documentation of version 0.2. This function should be
      avoided from now on. *)

  exception Unsafe_proj

  (** [safe_proj] <v,...,v> = v, raises the exception [Unsafe_proj]
      otherwise *)
  val safe_proj: 'a Bsml.par -> 'a
end
