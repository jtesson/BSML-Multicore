module Make = functor(Bsml : Bsmlsig.BSML) ->
struct
  module Base = Bsmlbase.Make(Bsml)
  module Comm = Bsmlcomm.Make(Bsml)
  open Bsml
  open Tools
  open Base 
  open Comm

  (***************************************************************)

  let rec drop n l = 
    if n<=0 
    then l 
    else drop (n-1) (List.tl l) 

  let rec take_every n stride = 
    function
	[]->[]
      |(h::t) ->
  	 if n<=1 
  	 then 
	   if n<1 then [] else [h]
  	 else  
	   h::(take_every (n-1) stride (drop (stride-1) t))

  let rec takecond cond = 
    function
	[] -> ([],[])
      | (h::t) as l-> 
      	  if  cond h 
      	  then 
	    let (l1,l2) = (takecond cond t) in (h::l1,l2)
      	  else ([],l)

  let partition_list compare pivots list =
    let rec aux i pivots list = 
      if pivots = [] 
      then [i,list]
      else
	let cond = fun x -> compare x (List.hd pivots) in
	  match list with
      	      [] -> []
	    |	list -> 
		  let ll = takecond cond list in 
		    (i,(fst ll))::(aux (i+1) (List.tl pivots) (snd ll)) in
      aux 0 pivots list


  let partition_array compare pivots keys =
    let lenp = (Array.length pivots) and lenk = (Array.length keys) in
    let pos = Array.create (lenp+2) 0 and list = ref [] in
      begin
	pos.(0)<-0; pos.(lenp+1)<-lenk;
	for i=1 to lenp do
	  while compare (keys.(pos.(i))) (pivots.(i-1)) do
      	    pos.(i)<-pos.(i)+1;
	  done;
	done;
	for i=lenp downto 0 do
	  list:=(i,(Array.sub keys (pos.(i)) (pos.(i+1)-pos.(i))))::!list;
	done;
	!list
      end

  (*****************************************************************************)

  (* val merge_array : ('a -> 'a -> bool) -> 'a array -> 'a array -> 'a array *)
  let merge_array compare a b =
    let la = Array.length a 
    and lb = Array.length b in
    let lab = la + lb in
      if lab = 0 
      then Array.sub a 0 0 
      else 
	let c = Array.create lab (Array.get  (if la>0 then a else b) 0) 
	and ia = ref 0 
	and ib = ref 0
	and ic = ref 0 in 
	  begin
	    while !ic<lab do 
      	      (if !ia >= la 
      	       then 
		 (c.(!ic)<- b.(!ib) ;
		  ib:=!ib+1)
      	       else
		 if !ib >= lb
		 then
		   (c.(!ic)<- a.(!ia) ;
		    ia:=!ia+1)
		 else 
		   if compare a.(!ia) b.(!ib) 
		   then 
		     (c.(!ic)<- a.(!ia) ;
		      ia:=!ia+1)
		   else
		     (c.(!ic)<- b.(!ib) ;
		      ib:=!ib+1));
	      ic:=!ic+1
	    done;
	    c
	  end

  (*****************************************************************************)

  exception Regular_sampling_sort

  (* val regular_sampling_sort_list :
     ('a -> 'a -> bool) -> 'a list Bsmlcore.par -> 'a list Bsmlcore.par *)
  (* This algorithm is described in 
     "Towards a Scalable Parallel Object Database"
     K R SUJITHAN -- PRG-TR-17-96 -- Oxford University *)
  let regular_sampling_sort_list compare vec =
    let p = bsp_p and int = int_of_float and float = float_of_int in
      (* val localy_sorted : 'a list Bsmlcore.par *)
    let localy_sorted = parfun (Sort.list compare) vec in
      (* val local_length :int list Bsmlcore.par *)
    let local_length = total_exchange ((parfun List.length) vec) in
      (* val global_length : int Bsmlcore.par *)
    let global_length = 
      parfun 
	(fun l -> let gl = List.fold_left (+) 0 l in 
	   if gl<p*p then raise Regular_sampling_sort else gl) 
	local_length in
      (* val sample_stride : int Bsmlcore.par *)
    let sample_stride = 
      parfun (fun gl -> int((float gl)/.(float(p*p)))) global_length 
	(* val number_of_samples : int Bsmlcore.par *)
    and number_of_samples = 
      (* val approximation :int list Bsmlcore.par *)
      let approximation = 
	apply2 
	  (replicate 
	     (fun gl ll->
		List.map
		  (fun len -> int ((float(len*p*p))/.((float gl)-.0.5)))
		  ll))
	  global_length 
	  local_length in 
	(* val approximation_sum :int Bsmlcore.par *)
      let approximation_sum = 
	parfun (List.fold_left (+) 0) approximation in
	apply2
	  (mkpar(fun pid sum ap->(List.nth ap pid)+(if pid<p*p-sum then 1 else 0)))
	  approximation_sum
	  approximation in
      (* val sent_samples : 'a list Bsmlcore.par*)
    let sent_samples = 
      put_one
	(apply3 
	   (replicate (fun nos ss ls -> 0,take_every nos ss ls))
	   number_of_samples
	   sample_stride
	   localy_sorted) in
      (* val sorted_samples : 'a list Bsmlcore.par *)
    let sorted_samples = 
      parfun (Sort.list compare) (parfun (List.fold_left (@) []) sent_samples) in
      (* val regular_pivots : 'a list Bsmlcore.par *)
    let regular_pivots =
      bcast_totex 
	0 
	(apply
	   (mkpar (function 0 ->take_every (p-1) p | _ ->  id))
	   (apply
	      (mkpar (function 0 ->drop ((p-2)+p/2) | _ -> id)) 
	      sorted_samples)) in 
      (* val distributed_partitions : 'a list list  Bsmlcore.par *)
    let distributed_partitions = 
      put_list
	(apply2 
	   (replicate (partition_list compare))
	   regular_pivots
	   localy_sorted) in
      parfun (List.fold_left (Sort.merge compare) []) distributed_partitions

let regular_sampling_sort_array compare vec =
  let p = bsp_p and int = int_of_float and float = float_of_int in
  (* val localy_sorted : 'a array Bsmlcore.par *)
  let localy_sorted = parfun (Array.sort compare) vec;vec in
  (* val local_length :int array Bsmlcore.par *)
  let local_length = total_exchange ((parfun Array.length) vec) in
  (* val global_length : int Bsmlcore.par *)
  let global_length = 
    parfun 
      (fun l -> let gl = List.fold_left (+) 0 l in 
	if gl<p*p then raise Regular_sampling_sort else gl) 
      local_length in
  (* val sample_stride : int Bsmlcore.par *)
  let sample_stride = 
    parfun (fun gl -> int((float gl)/.(float(p*p)))) global_length 
  (* val number_of_samples : int Bsmlcore.par *)
  and number_of_samples = 
    (* val approximation :int array Bsmlcore.par *)
    let approximation = 
      apply2 
	(replicate 
	   (fun gl ll->
	     List.map
	       (fun len -> int ((float(len*p*p))/.((float gl)-.0.5)))
	       ll))
	global_length 
	local_length in 
    (* val approximation_sum :int Bsmlcore.par *)
    let approximation_sum = 
      parfun (List.fold_left (+) 0) approximation in
    apply2
      (mkpar(fun pid sum ap->(List.nth ap pid)+(if pid<p*p-sum then 1 else 0)))
      approximation_sum
      approximation in 
 (* val sent_samples : (int * 'a array) Bsmlcore.par*)
  let sent_samples = 
    put_one
      (apply3 
	 (replicate (fun nos ss ls -> 0,
	   let samples = Array.create nos ls.(0) in 
	   for i=1 to (nos-1) do 
	     Array.set samples i (ls.(i*ss));
	   done; samples))
	 number_of_samples
	 sample_stride
	 localy_sorted) in
  (* val sorted_samples : 'a array Bsmlcore.par *)
  let sorted_samples = 
    let tmpa = parfun Array.concat sent_samples in
      (parfun (Array.sort compare) tmpa);tmpa in 
  (* val regular_pivots : 'a array Bsmlcore.par *)
  let regular_pivots =
    bcast_totex 
      0 
      (apply
	 (mkpar 
	    (function 
	     	0 ->
		  fun ss -> 
		    let pivots = Array.create (p-1) (ss.((p-1)+(p/2))) in 
		    for i=1 to (p-1) do 
		      Array.set pivots (i-1) (ss.(i*p+(p/2)-1));
		    done; pivots 
	      |	_ -> id))
       sorted_samples) in
  (* val distributed_partitions : 'a array array  Bsmlcore.par *)
  let distributed_partitions = 
    put_list
      (apply2 
	 (replicate (partition_array (fun x y->compare x y<0)))
	 regular_pivots
	 localy_sorted) in
  apply2
    (replicate (List.fold_left (merge_array (fun x y->compare x y<0))))
    (parfun (fun a->Array.sub a 0 0) vec)
    distributed_partitions
end
