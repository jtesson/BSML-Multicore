(*********************************************************************)

let natmod i m = (m+(i mod m)) mod m

let rec from_to n1 n2 = if n1>n2 then [] else n1::(from_to (n1+1) n2)

let rec filtermap p f = function 
    [] -> []
  | h::t -> 
      if p h 
      then (f h)::(filtermap p f t)
      else filtermap p f t 

let is_Some = function Some _ -> true | None -> false

let noSome (Some x) = x

let none = fun x -> None

let id x = x

let mklist x = [x]

let compose f g x = f (g x)

(*********************************************************************)








