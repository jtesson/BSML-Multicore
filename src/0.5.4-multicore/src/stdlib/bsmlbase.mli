module Make : 
  functor (Bsml : Bsmlsig.BSML) ->
sig
  (** Very often used functions *)

  (** [replicate x] gives a parallel vector with the value [x] on each
      process. *)
  val replicate : 'a -> 'a Bsml.par

  (** [parfun f] <x{_0},...,x{_(p-1)}> = <f x{_0},...,f x{_(p-1)}> *)
  val parfun : ('a -> 'b) -> 'a Bsml.par -> 'b Bsml.par

  (** Same thing as [parfun] but with a function of arity 2, 3 or
      4. *)

  val parfun2 :
    ('a -> 'b -> 'c) -> 
    'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par

  val parfun3 :
    ('a -> 'b -> 'c -> 'd) ->
    'a Bsml.par -> 'b Bsml.par -> 
    'c Bsml.par -> 'd Bsml.par

  val parfun4 :
    ('a -> 'b -> 'c -> 'd -> 'e) ->
    'a Bsml.par ->'b Bsml.par -> 
    'c Bsml.par -> 'd Bsml.par -> 'e Bsml.par

  (** Same thing as [apply] but with a function of arity 2, 3 or
      4. *)

  val apply2 :
    ('a -> 'b -> 'c) Bsml.par ->
    'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par
  val apply3 :
    ('a -> 'b -> 'c -> 'd) Bsml.par ->
    'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par -> 'd Bsml.par
  val apply4 :
    ('a -> 'b -> 'c -> 'd -> 'e) Bsml.par ->
    'a Bsml.par ->
    'b Bsml.par -> 'c Bsml.par -> 'd Bsml.par -> 'e Bsml.par


  val mask: (int->bool) -> 'a Bsml.par -> 'a Bsml.par -> 'a Bsml.par

  (** [applyat] n f{_1} f{_2} v applies function f{_1} at process {i
      n} and f{_2} otherwise *)
  val applyat: int -> ('a->'b) -> ('a->'b) -> 'a Bsml.par -> 'b Bsml.par

  (** [applyif] b f{_1} f{_2} v applies function f{_1} at process {i
      n} if (b n) is true and f{_2} otherwise *)
  val applyif: (int->bool) -> ('a->'b) -> ('a->'b) 
    -> 'a Bsml.par -> 'b Bsml.par

  (** [procs] is the list of the process numbers *)
  val procs : int list

  (** [this] is the parallel vector such as each process hold its
      number*)
  val this : int Bsml.par

  (** [bsml_print print_element pid element] prints the value of
      [element] at process [pid] using the printer
      [print_element] *)
  val bsml_print : ('a -> unit) -> int -> 'a Bsml.par -> unit Bsml.par

  (** [parprint print v] print the parallel vector [v] using the
      printer [print], one line per process, each line beginning with
      the number of the process. For example, [(parprint print_int
      (this()))] will give the following for 4 processes: {[
      0: 0
      1: 0
      2: 0
      3: 0
      ]} *)
  val parprint : ('a -> unit) -> 'a Bsml.par -> unit Bsml.par

  (** [get_one] <x{_0},...,x{_p-1}> <i{_0},...,i{_p-1}> evaluates to
      <x{_i_0},...,x{_i_p-1}>. The process numbers are considered
      module {i p} *)
  val get_one : 'a Bsml.par -> int Bsml.par -> 'a Bsml.par

  (** The order of the elements of the result list is the same as
      the order of the process numbers in the argument list. *)
  val get_list : 'a Bsml.par -> int list Bsml.par -> 'a list Bsml.par

  (** Each process holds a pair [(dst,v)] where [dst] is the number of
      the process of destination and [v] the value to send.  If [dst]
      is not a valid process number, it is ignored. The result list is
      ordered by source process. *)
  val put_one : (int * 'a) Bsml.par -> 'a list Bsml.par 

  (** Each process holds an association liste of pairs [(dst,v)]
      where [dst] is the number of the process of destination and
      [v] the value to send.  If [dst] is not a valid process
      number, it is ignored. If there are two pairs with the same
      key, only the first is considered. *)
  val put_list :
    (int * 'a ) list Bsml.par -> 'a list Bsml.par

  (** [proj_list_pids] returns a (int * 'a) list in which each first
      couple element is the number of the proc holding the 'a value *)
  val proj_list_pids : 'a Bsml.par -> (int * 'a) list
end
