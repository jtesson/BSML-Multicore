module Make =  
functor(Bsml : Bsmlsig.BSML) ->
struct

  open Bsml
  open Tools
  module Base = Bsmlbase.Make(Bsml)
  open Base

  (*********************************************************************)

  let noSome(Some x) = x

  let shift dec datas =
    let mkmsg = fun pid data dst -> 
      if dst=(natmod  (dec+pid) bsp_p) 
      then Some data
      else None in
      apply (parfun (compose noSome) (put(apply (mkpar mkmsg) datas)))
	(mkpar(fun pid->natmod (pid-dec) bsp_p))

  let shift_right vec = shift 1 vec

  let shift_left vec = shift (-1) vec

  (*********************************************************************)

  let totex vv = 
    parfun (compose noSome) (put(parfun (fun v dst->Some v) vv))

  let total_exchange vec =
    parfun2 List.map (totex vec) (replicate procs)

  (*********************************************************************)

  exception Scatter

  let scatter partition root v =
    if not (within_bounds root) 
    then raise Scatter
    else
      let mkmsg = mkpar(fun pid->if pid=root 
			then partition else fun v _->None) in
      let msg = put (apply mkmsg v) in
	parfun noSome (apply msg (replicate root))

  let rec separation n l = 
    if n=0
    then ([],l)
    else 
      let (l1,l2) = separation (n-1) (List.tl l)
      in (List.hd l)::l1,l2

  let head n l = fst (separation n l)

  let tail n l = snd (separation n l)

  let cut_list l = 
    let totlen = List.length l in (* longueur totale de la liste l*)
    let len  = totlen / bsp_p (* longueur basse des morceaux *)
    and nb = totlen mod bsp_p (* nb de morceaux de taille len+1 *) in
      fun i -> if i<nb
      then Some (head (len+1) (tail (i*(len+1)) l))
      else Some (head len (tail (nb*(len+1)+(i-nb)*len) l))

  let scatter_list root vl = scatter cut_list root vl

  let cut_array_or_string length sub x = 
    let totlen = length x in 
    let len  = totlen / bsp_p 
    and nb = totlen mod bsp_p in
      fun i -> if i<nb
      then Some (sub x (i*(len+1)) (len+1))
      else Some (sub x (nb*(len+1)+(i-nb)*len) len)
	
  let scatter_array root va = 
    scatter (cut_array_or_string Array.length Array.sub) root va

  let scatter_string root vs = 
    scatter (cut_array_or_string String.length String.sub) root vs

  (*********************************************************************)

  exception Gather

  let gather root vv =
    let mkmsg pid v dest = if dest=root then Some v else None in
      put(apply (mkpar mkmsg) vv)

  let gather_list root vv =
    let procs_at_root = mkpar(fun i->if i=root then procs else []) in
    let to_list = parfun (compose noSome) (gather root vv) in
      parfun2 List.map to_list procs_at_root

  (*********************************************************************)

  exception Bcast

  let bcast_direct root vv = 
    if not (within_bounds root) then raise Bcast else
      let mkmsg = mkpar(fun pid v dst->if pid=root then Some v else None) in
	parfun noSome (apply (put (apply mkmsg vv)) (replicate root))

  let bcast_totex_gen partition paste root vv =
    if not (within_bounds root) then raise Bcast else
      let phase1 = scatter partition root vv in
      let phase2 = totex phase1 in
	parfun paste phase2

  let bcast_totex_list root vl =
    let paste f = List.flatten (List.map f procs) in
      bcast_totex_gen cut_list paste root vl 

  let bcast_totex_array root va =
    let paste f = Array.concat (List.map f procs) in
      bcast_totex_gen (cut_array_or_string Array.length Array.sub) paste root va

  let bcast_totex_string root vs =
    let paste f = String.concat "" (List.map f procs) in
      bcast_totex_gen 
	(cut_array_or_string String.length String.sub) 
	paste root vs

  let bcast_totex root vv = 
    let input_data = parfun (fun v->Marshal.to_string v [Marshal.Closures]) vv in
    let output_data = bcast_totex_string root input_data in
      parfun2 (Marshal.from_string) output_data (replicate 0)

  (*********************************************************************)

  let scan_direct op vv = 
    let mkmsg pid v dst = if dst<pid then None else Some v in
    let procs_lists = mkpar(fun pid->from_to 0 pid) in
    let receivedmsgs = put(apply(mkpar mkmsg) vv) in
    let values_lists = parfun2 List.map 
      (parfun (compose noSome) receivedmsgs) procs_lists in
      parfun (fun (h::t)->List.fold_left op h t) values_lists

  (* A VERIFIER *)
  let scan_logp op vec =
    let rec scan_aux n vec =
      if n >= bsp_p 
      then vec
      else 
	let msg = mkpar(fun pid v dst->
			  if ((dst=pid+n)or(pid mod (2*n)=0))
			    & (within_bounds (dst-n))
			  then Some v else None)
	and senders = mkpar(fun pid-> natmod (pid-n) bsp_p)
	and op' = fun x y->match y with Some y'->op x y'|None -> x in
	let vec' = apply (put(apply msg vec)) senders in 
	let vec''= parfun2 op' vec vec' in
	  scan_aux (n*2) vec'' in
      scan_aux 1 vec

  let scan_wide scan seq_scan last_element map op vv =
    let local_scan = parfun (seq_scan op) vv in
    let last_elements = parfun last_element local_scan in
    let values_to_add = shift_right (scan op last_elements) in
    let pop = applyat 0 (fun x y->y) op in
      parfun2 map (pop values_to_add) local_scan

  let scan_wide_direct seq_scan last_element map op vv =
    scan_wide scan_direct seq_scan last_element map op vv

  let scan_wide_logp seq_scan last_element map op vv =
    scan_wide scan_logp seq_scan last_element map op vv

  let scan_list scan op vl = 
    let rec seq_scan op = 
      function 
	  [] -> [] 
	| h::t -> h::(List.map (op h) (seq_scan op t))   
    and last_element l = List.hd(List.rev l) in
      scan_wide scan seq_scan last_element List.map op vl

  let scan_array scan op va = 
    let seq_scan op a = 
      let len=Array.length a in
      let a'=Array.init len (fun i->a.(i)) in
	for i=1 to (len-1) do 
	  Array.set a' i (op (a'.(i-1)) (a'.(i))) 
	done;a' 
    and last_element a = a.((Array.length a)-1) in
      scan_wide scan seq_scan last_element Array.map op va

  let scan_list_direct op vl = scan_list scan_direct op vl
  and scan_list_logp op vl = scan_list scan_logp op vl
  and scan_array_direct op va = scan_array scan_direct op va
  and scan_array_logp op va = scan_array scan_logp op va 

  (*********************************************************************)

  let fold_direct op vec = 
    parfun (function h::t->List.fold_left op h t) (total_exchange vec)

  let fold_wide par_fold local_fold op vec =
    let local_folded = parfun (local_fold op) vec in
      par_fold op local_folded

  let fold_list fold op vl =
    let seq_fold op (h::t) = List.fold_left op h t in
      fold_wide fold seq_fold op vl

  let fold_array fold op va = 
    let seq_fold = fun op array -> Array.fold_left op 
      (Array.get array 0) 
      (Array.sub array 1 (Array.length array - 1))
    in fold_wide fold seq_fold op va

  let fold_logp op vec = 
    bcast_totex (bsp_p-1) (scan_logp op vec)

  let fold_list_direct op vl = fold_list fold_direct op vl
  and fold_list_logp op vl = fold_list fold_logp op vl
  and fold_array_direct op va = fold_array fold_logp op va
  and fold_array_logp op va = fold_array fold_logp op va

  (*********************************************************************)
end
