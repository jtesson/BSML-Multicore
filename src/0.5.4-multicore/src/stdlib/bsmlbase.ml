module Make = functor (Bsml : Bsmlsig.BSML) ->
struct

  open Tools

  open Bsml

  let replicate x = mkpar (fun pid->x)

  let parfun f v = apply (replicate f) v
    
  let parfun2 f v1 v2 = apply (parfun f v1) v2

  let parfun3 f v1 v2 v3 = apply (parfun2 f v1 v2) v3

  let parfun4 f v1 v2 v3 v4 = apply (parfun3 f v1 v2 v3) v4

  let apply2 f v1 v2 = apply (apply f v1) v2

  let apply3 f v1 v2 v3 = apply (apply2 f v1 v2) v3

  let apply4 f v1 v2 v3 v4 = apply (apply3 f v1 v2 v3) v4

  let mask p v1 v2 = 
    apply2 (mkpar(fun i x y->if (p i) then x else y)) v1 v2

  let applyif p f1 f2 v =
    apply (mkpar(fun i->if (p i) then f1 else f2)) v

  let applyat n f1 f2 v = 
    applyif (fun i->i=n) f1 f2 v

  (*************************************************************)

  let procs = from_to 0 (bsp_p-1)

  let this = mkpar(fun pid->pid) 

  (*********************************************************************)

  let bsml_print p at_pid vec =
    let nothing x = () in
      apply (mkpar(fun pid->if pid=at_pid then p else nothing)) vec

  let parprint p vec = 
    parfun2 
      (fun pid data -> 
	 print_string "Process ";
	 print_int pid; print_string " : "; 
	 p data;
	 print_newline())
      this
      vec

  (*********************************************************************)

  let get_one datas srcs =
    let pids = parfun (fun i->natmod i bsp_p) srcs in
    let ask = put(parfun (fun i dst->if dst=i then Some() else None) pids) 
    and replace_by_data =
      parfun2 (fun f d dst->match(f dst)with Some() -> Some d|_->None) in
    let reply = put(replace_by_data ask datas) in
      parfun (fun(Some x)->x) (apply reply pids)

  let get_list datas lsrcs =
    let lpids = parfun (List.map (fun i->natmod i bsp_p)) lsrcs in 
    let ask = 
      put (parfun (fun l dst->if List.mem dst l then Some() else None) lpids) 
    and replace_by_data =
      parfun2 (fun f d dst->match(f dst)with Some() -> Some d|_->None) in
    let reply = put(replace_by_data ask datas) in
      parfun (List.map (fun(Some x)->x)) (parfun2 List.map reply lpids)

  (*********************************************************************)

  let put_one dst_and_datas = 
    let msgs = 
      parfun 
	(fun (dst,data) dstpid ->if dstpid=dst then Some data else None)
	dst_and_datas in
    let rec to_list l f = match l with
	[] -> []
      | h::t -> match (f h) with 
	    Some x -> x::(to_list t f) 
	  |	None -> (to_list t f) in
      parfun (to_list procs) (put msgs) 
	
  let put_list ldst_and_datas =
    let msgs = parfun 
      (fun list dst-> 
	 try Some (List.assoc dst list)
	 with Not_found -> None)  
      ldst_and_datas in
    let rec to_list l f = match l with
	[] -> []
      | h::t -> match (f h) with 
	    Some x -> x::(to_list t f) 
	  |	None -> (to_list t f) in
      parfun (to_list procs) (put msgs) 
	
(*********************************************************************)

  let proj_list_pids v = List.map (fun i -> i, proj v i) procs

(*********************************************************************)
end
