module Make :
functor(Bsml : Bsmlsig.BSML) ->
sig
  (** Sorting *)

  (** [regular_sampling_sort cmp list] sorts the list (or array) with
      respect to the order given by [cmp].  {% The regular sampling
      BSP algorithm is described in
      \cite{SujithanKR1996a,Sujithan1997}.%} This sort requires that
      the total number of elements be greater than {i
      p}{^2}. Otherwise [Regular_sampling_sort] is raised. The regular
      sampling sort insures that at the end of the sort each processor
      will contains at most 2*{i n/p} elements, where {i n} is the
      total number of elements. *)

  exception Regular_sampling_sort
  val regular_sampling_sort_list :
    ('a -> 'a -> bool) -> 'a list Bsml.par -> 
    'a list Bsml.par 
  val regular_sampling_sort_array :
    ('a -> 'a -> int) -> 'a array Bsml.par -> 
    'a array Bsml.par 
end
