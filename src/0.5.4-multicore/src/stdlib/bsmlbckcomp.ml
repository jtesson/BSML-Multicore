module Make = functor (Bsml : Bsmlsig.BSML) ->
struct
  module Base = Bsmlbase.Make(Bsml)
  module Comm = Bsmlcomm.Make(Bsml)
  open Bsml
  open Tools
  open Base 
  open Comm

  (*******************************************************************)

  let bsml_begin () = ()
  let bsml_end () = ()

  (*******************************************************************)

  let divides m n = (n mod m = 0)
  let square_root n = truncate(sqrt(float n))

  let is_prime n =
    let i = ref 2 in
    let racine = square_root n in
    let prime = ref true in
      if n < 2 then
	false
      else
	begin
	  while !i <= racine && !prime do
      	    if divides !i n then
      	      prime := false
      	    else
      	      incr i
	  done;
	  !prime
	end

  let rec first_prime_from m =
    if is_prime m then m else first_prime_from (m+1)

  let rec liste_sans_doubles_adjacents = function
    | [] -> true
    | [e] -> true
    | e::(f::l as k) -> (e <> f) && (liste_sans_doubles_adjacents k)

  let test_validity exn msg2 vl = 
    let verify pid l = 
      if not (List.for_all within_bounds l) 
      then raise(exn("Out of bounds : node "^ 
		       (string_of_int pid)^
		       "looks for out of bound node."));
      if not (liste_sans_doubles_adjacents (Sort.list (<) l)) 
      then raise(exn(msg2^(string_of_int pid))) in
      Bsml.apply (Bsml.mkpar verify) vl

  let create_table_from_list l = 
    let table = Hashtbl.create (first_prime_from (List.length l)) in
    let add (i,v) = Hashtbl.add table i v in
      List.iter add l;
      table
	
  exception Get_failure of string

  let get_failure s = Get_failure s

  let get datas lpids =
    test_validity get_failure "Multiple get of same value by node " lpids; 
    let fask = fun l dst->if List.mem dst l then Some() else None in
    let ask = Bsml.put (parfun fask lpids) 
    and replace_by_data =
      parfun2 (fun f d dst->match(f dst)with Some() -> Some d|_->None) in
    let reply = Bsml.put(replace_by_data ask datas) in
    let assoc_list =
      parfun2 (List.map2 (fun  i (Some x) ->(i,x))) 
	lpids 
	(parfun2 List.map reply lpids) in
      parfun create_table_from_list assoc_list

  (*******************************************************************)

  exception Put_failure of string

  let put_failure s = Put_failure s

  let put ldst_and_datas =
    test_validity put_failure "Multiple put to one destination from node " 
      (parfun (List.map fst) ldst_and_datas); 
    let assoc_list = 
      let msgs = parfun 
	(fun list dst-> 
	   try Some (List.assoc dst list)
	   with Not_found -> None)  
	ldst_and_datas in
      let rec to_list l f = match l with
	  [] -> []
	| h::t -> match (f h) with 
	      Some x -> (h,x)::(to_list t f) 
	    |	None -> (to_list t f) in
	parfun (to_list procs) (Bsml.put msgs) in
      parfun create_table_from_list assoc_list
	
  (*******************************************************************)

  let bsml_abort_string s = 
    Bsml.abort 1 s

  (*******************************************************************)

  let option_from_list fl x = 
    fun dst -> 
      try Some (List.assoc dst (fl x))
      with Not_found -> None

  let scatter partition root v =
    scatter (option_from_list partition) root v

  (*******************************************************************)
  exception Unsafe_proj

  let safe_proj v = 
    let check (h::t) = 
      let rec aux x = 
	function 
	    [] -> true 
	  | h::t when h=x-> aux x t in
	aux h t in
    let f = proj v in
    let l = List.map f procs in
      if (check l) then List.hd l else raise Unsafe_proj
end
