(** Useful sequential functions *)

val natmod: int -> int -> int
(** Modulo *)

val from_to: int -> int -> int list
(** [from_to n1 n2] = [[n1;n1+1;...;n2]] *)

val is_Some: 'a option -> bool
(** [isSome v] is [true] if [v]=[Some v'], [false] otherwise *)

val filtermap: ('a -> bool) -> ('a -> 'b) -> 'a list -> 'b list
(** [filtermap p f l] applies [f] to each element of [l] which satifies the 
predicate [p] *)

val none: 'a -> 'b option
(** Constant function which always returns [None] *)

val noSome: 'a option -> 'a 

val compose: ('a -> 'b) -> ('c -> 'a) -> 'c -> 'b
val id: 'a -> 'a
val mklist: 'a -> 'a list
(** [mklist v]=[[v]] *)
