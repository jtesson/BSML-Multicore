let merge (inner :int -> 'a -> unit) (outter:int -> 'a -> unit) =
  fun (dest:int) (s:'a) -> (inner dest s ;outter dest s) 

let inner =  fun (i:int) s -> ()
let outter = fun (i:int) s -> ()

type 'a par = Par of 'a 				

let apply (Par f) (Par v) = Par (f v)
let mkpar f  = Par (f 0)

let build_vect :  'b.(int -> (int -> 'b -> unit) -> (int -> 'b -> unit) -> int -> 'b -> unit)  =
  fun i inn out -> merge inn out
let vect_merge = Obj.magic( mkpar (fun i -> build_vect i))
			  
let vect_merge :
      ((int -> 'a -> unit) -> (int -> 'a -> unit) -> (int -> 'a -> unit)) par
  =  vect_merge

let vect_merge :
      ((int -> 'a -> unit) -> (int -> 'a -> unit) -> (int -> 'a -> unit)) par
  =  let vect_merge : 'b = Obj.magic( mkpar (fun i -> build_vect i))
			  
     in vect_merge
let merging  = apply ( apply (mkpar (build_vect) ) inner ) outter

		 
let res :  'a->int =  (fun (x:'a) -> (fun x -> 1) (Obj.magic x))
let res : bytes =  (Marshal.to_bytes (fun x -> 1) [] )
let res : 'a->int = Marshal.from_bytes res 0

    )
  
