(*pp $PP *)
open Bsmlsig
open BsmlInstrumentation
module Seq_time  (* :INSTRUMENTATION *) = functor  (Bsml:BSML) ->
	  struct

            module  BsmlRaw = Bsml
	    type logs_local = float list * float option * float 

	    type logs_global = unit

	    let names = ["local_sequential_timing"]

	    (* Initialy, the counter is stopped *)
	    let init () = << [], None ,0. >> , ()
				    
	    (* Initialy, the counter is stopped *)
	    let reinit (loc_logs, glob_log) =
	      << [], None ,0. >> , ()

	    (* when having a break, 
                    the current time spent is accumulated (if counter on)
                    and 
                     the counter is stopped 
                The counter should normally be off when pause is called by the system (comming from outside vector)
	     *)
	    let pause (loc_logs, glob_log) =
	      <<
	       let stop_time = Sys.time() in
	       let (time_log, last_timestamp , last_timing) = $loc_logs$ in
	       match last_timestamp with 
	       None  -> 
	       (time_log, None , last_timing)
	       | Some last_timestamp ->
	              (time_log, Some 0. , (stop_time -. last_timestamp)+. last_timing)
	       >>,
	      glob_log
		
	    (* when resuming, 
                    the accumulator and time logs are copied
                    and 
                    the counter is started *)
	    let resume (loc_logs, glob_log) =
	      <<
	       let (time_log, last_timestamp , last_timing) = $loc_logs$ in
	       match last_timestamp with 
	       None -> (time_log, None , last_timing)
	       | Some 0. -> (time_log, Some (Sys.time()), last_timing)
	       | _ -> failwith "Seq_time instrumentation failure"
	       >> , 
	      glob_log
		

	    (* when starting a local computation, 
                    the accumulator and time logs are copied
                    and 
                    the counter is started *)
	    let logging_seq_start i (time_log, _ ,last_timing) =
	      (time_log, Some (Sys.time()),last_timing)
		
	    (* when ending a local computation, 
                    the current time spent is accumulated (if counter on)
                    and 
                    the counter is stopped *)
	    let logging_seq_stop i s (time_log,last_timestamp,last_timing) =
	      match last_timestamp with
		None -> failwith  "Seq_time instrumentation failure"
	       | Some last_timestamp ->
		  let stop_time = Sys.time() in
		  (time_log,None,(stop_time -. last_timestamp)+. last_timing)

	    (* In and out comms are ignored (log is copied) *)
	    let logging_comm_out _ _ _ logs = logs
	    let logging_comm_in _ _ _ logs = logs
					       
	    (* Before synchronisation, 
                    the current  accumulated time is pushed on the timer stack
                    and 
                    the accumulator is reset to 0 *)
	    let logging_before_sync_local _  (time_log,last_timestamp,last_timing) =
	      (last_timing::time_log, last_timestamp, 0.)

	    (* After synchronisation (global and local), logs are copied *)
	    let logging_after_sync_local _  logs = logs
	    let logging_sync_global logs = logs
			  
	  end		   

module Replicated_time  (* :INSTRUMENTATION *)= functor  (Bsml:BSML) ->
  struct
    type logs_local = float list * float option  * float
    type logs_global = unit

    let paused = ref false
		     
    (* Initialy, the counter is started *)
    let init () = << [], Some (Sys.time()),0. >> , ()
					     

    let names = ["global_replicated_timing"]
    (* Initialy, the counter is started *)
    let reinit (loc_logs, glob_log) =
      << [],Some (Sys.time()),0. >> , ()

	
    (* when having a break, 
                    the current time spent is accumulated (if counter on)
                    and 
                    the counter is stopped *)
    let pause (loc_logs, glob_log) =
      paused:=true;
      <<  let (time_log, last_timestamp , last_timing) = $loc_logs$ in
       match last_timestamp with
	None -> 
       (time_log, last_timestamp , last_timing)
      | Some last_timestamp ->
        (* print_endline (" Pause (accumulating)  at " ^(string_of_int $this$)); *)
         let stop_time = Sys.time() in
          (time_log, None , (stop_time -. last_timestamp)+. last_timing)
       >>,
      glob_log


		
    (* when resuming, 
               the accumulator and time logs are copied
               and 
               the counter is started *)
    let resume (loc_logs, glob_log) =
      paused:=false;
      <<	
       (* print_endline (" resume (reset timestamp) at " ^(string_of_int $this$)); *)
       let (time_log, _ , last_timing) = $loc_logs$ in
       (time_log, Some (Sys.time()) , last_timing)
       >>,
      glob_log
	
    let logging_seq_start i (time_log, last_timestamp , last_timing) =
      match last_timestamp with
	None ->
	(* print_endline (" sstart paused at " ^(string_of_int i)); *)
	(time_log, last_timestamp , last_timing)
      | Some last_timestamp ->
	 (* print_endline (" sstart accumuluating at " ^(string_of_int i)); *)
         let stop_time = Sys.time() in
	 (time_log,None,(stop_time -. last_timestamp)+. last_timing)
	
    let logging_seq_stop i s (time_log, timestamp ,last_timing) =
      if !paused
      then
	begin
	  (* print_endline (" Sstop (from paused)  at " ^(string_of_int i)); *)
	  (time_log, timestamp ,last_timing)
	end
      else
	begin
	  (* print_endline (" Sstop   at " ^(string_of_int i)); *)
	  (time_log, Some (Sys.time()),last_timing)
	end

    let logging_comm_out _ _ _ logs = logs
    let logging_comm_in _ _ _ logs = logs
				       
    let logging_before_sync_local i  (time_log,last_timestamp,last_timing) =
      match last_timestamp with
	None ->
	(* print_endline (" before  (push timing) at " ^(string_of_int i)); *)
	(last_timing::time_log, None, 0.)
      | _ -> failwith "Replicated_time instrumentation failure at before_sync, shoud be paused"
												
    let logging_after_sync_local _  logs = logs

    let logging_sync_global (loc_logs, glob_log) =
      << 
       (* print_endline (" Gsync  (copy timing, reset  timestamp) at " ^(string_of_int $this$)); *)
       let tl,_,_ = $loc_logs$ in tl,Some (Sys.time()),0. >> , glob_log
				     
  end		   
