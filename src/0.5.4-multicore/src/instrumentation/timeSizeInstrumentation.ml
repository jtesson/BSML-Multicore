(*pp $PP *)
open BsmlInstrumentation

       
module Time_cumulated_size  (Sz_of : sig val size_of_data : 'a -> int end )  =
  Merge (TimeInstrumentation.Seq_time )
	(SizeInstrumentation.Cumulated_size (Sz_of) ) 
	
module Time_Histogrammes_size  (Sz_of : sig val size_of_data : 'a -> int end )  =
  Merge (TimeInstrumentation.Seq_time )
	(SizeInstrumentation.Histogrammes_size (Sz_of) )
  
