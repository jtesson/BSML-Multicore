(*pp $PP *)
open Bsmlsig
open BsmlInstrumentation
open Tools

 
module Cumulated_size (* : INSTRUMENTATION *)
(Sz_of : sig val size_of_data : 'a -> int end )  (Bsml:BSML)
=
	  struct


            module  BsmlRaw = Bsml
            open Sz_of
 	    
	    type logs_local = int * int
	    type logs_global = int list

	    let names = ["cumulated_size"]
				   
	    let init () = << 0,0 >> , []
	    let reinit _ = << 0,0 >> , []

	    let pause (loc,glob) = (loc,glob)
	    let resume (loc,glob) = (loc,glob)
				      
	    let logging_seq_start i logs = logs

	    let logging_seq_stop i s logs = logs
					 
	    let logging_comm_out i dest data (h_plus,h_moins) =
	      let sz =  (size_of_data data) in
	      (h_plus + sz , h_moins)
		    
	    let logging_comm_in  i dest data (h_plus,h_moins) =
	      let sz =  (size_of_data data) in
	      (h_plus, h_moins + sz)
		
	    let logging_before_sync_local this logs =
	      let printing () =
		print_endline ( "Proc"^(string_of_int this)^
				  " h+ = "^(string_of_int (fst logs))^
				    " h- = "^(string_of_int (snd logs))
			      )
	      in
	      (* printing (); *)
	      logs

	    let logging_after_sync_local this logs = logs
	    
	      let logging_sync_global (loc_logs, glob_log) =
	      let comms = Bsml.proj loc_logs in
	      let rec aux this comms =
		let hp,hm = comms this in
		max
		  (max hp hm)
		  (if this = Bsml.bsp_p-1 then 0  else aux (this+1) comms)
	      in
	      (<< (0,0) >> , (aux 0 comms)::glob_log)	  
	  end
	    
module Histogrammes_size  (* : INSTRUMENTATION *)
       (Sz_of : sig val size_of_data : 'a -> int end )
       (Bsml:BSML)
       =      
	  struct

            open Sz_of
	    open Bsml

	    let names = ["Histogrammes_size"]
	    type logs_local = int array
	    type logs_global = int array array list
				   
	    let init () = << Array.make bsp_p 0 >> , []

	    let reinit (loc_histogram, matrix_histo) =
	      << let hp = $loc_histogram$ in
	       Array.fill hp 0 bsp_p  0 ; hp >> , [] 

	    let pause (loc,glob) = (loc,glob)
	    let resume (loc,glob) = (loc,glob)

	    let logging_seq_start i logs = logs
										     
	    let logging_seq_stop i s logs = logs
					 
	    let logging_comm_out i dest data (hp) =
	      (hp.(dest)<- hp.(dest) + (size_of_data data) ; hp)
		    
	    let logging_comm_in  i dest data (hp) = hp
		
	    let logging_before_sync_local this logs = logs
	    let logging_after_sync_local this logs = logs
						 
	    let logging_sync_global (loc_logs, glob_log) =
	      let comms = Bsml.proj loc_logs in
	      let matrix =
		Array.init
		  bsp_p
		  ( fun from ->
		    Array.init
		      bsp_p
		      ( fun dest ->
			(comms from).(dest)
		      )
		  )
	      in
	      let printing () =
		print_endline (string_of_matrix string_of_int matrix)
	      in
	      (* printing () ; *)
	      ( << let hp = $loc_logs$ in
		 Array.fill hp 0 bsp_p  0 ; hp >> ,
		matrix::glob_log
	      )
	  end
	    
	
