(*pp $PP *)
open Tools
open Bsmlsig
       

module type INSTRUMENTATION = functor (B:BSML) ->
	  sig
	    type logs_local
	    type logs_global
	    val names: string list

	    (* initial logs given by the user *)
	    val init : unit -> (logs_local B.par * logs_global)
						      
	    (* Reinitialisation of logs by the user *)
	    val reinit : (logs_local B.par) * logs_global -> (logs_local B.par * logs_global)

	    (* Reinitialisation of logs by the user *)
	    val pause : (logs_local B.par) * logs_global -> (logs_local B.par * logs_global)
	    (* Reinitialisation of logs by the user *)
	    val resume : (logs_local B.par) * logs_global -> (logs_local B.par * logs_global)
										    
	    (** logging handler for start of sequential computation 
                The Instrumented bsml ensures that the next instrumentation 
                function called will be logging_seq_stop.
                
                arguments are the proc number and a the local log
                returns the new local log
	     *)
	    val logging_seq_start : (int -> logs_local -> logs_local)
						   
	    (** logging handler for the end of sequential computation  
               The Instrumented bsml ensures that the previously instrumentation 
               function called was logging_seq_start.
                
               arguments are the proc number, 
                             the locally computed value and 
                             the local log
               returns the new local log
	     *)
	    val logging_seq_stop : (int -> 'a -> logs_local -> logs_local)
				     
	    (** logging handler for output communication  
               arguments are the proc number, 
                             the destination processor number;
                             the communicated value and 
                             the local log
               returns the new local log
	     *)

 	    (** handler called on each proc after syncronisation  *)
	    val logging_before_sync_local  : (int -> logs_local -> logs_local)

	    val logging_comm_out : (int -> int -> 'a -> logs_local -> logs_local)
				     
	    (** logging handler for input communication  
               arguments are the proc number, 
                             the sender's processor number;
                             the received value and 
                             the local log
               returns the new local log
	     *)
	    val logging_comm_in  : (int -> int -> 'a -> logs_local -> logs_local)

 	    (** handler called on each proc after syncronisation  *)
	    val logging_after_sync_local  : (int -> logs_local -> logs_local)
	    (* handler called at global level after syncronisation  *)
	    val logging_sync_global : (logs_local B.par) * logs_global -> (logs_local B.par) * logs_global
  end
		
module Make  (BsmlRaw:BSML) (Instr:INSTRUMENTATION) :
(INSTRUMENTED_BSML with
   type logs = Instr(BsmlRaw).logs_local BsmlRaw.par * Instr(BsmlRaw).logs_global 
 and
   type 'a par  = 'a BsmlRaw.par
and
  module Raw = BsmlRaw
)
  =	
  struct
    module Bsml = BsmlRaw   
    include BsmlRaw
    module I = Instr(BsmlRaw)

    type logs = (I.logs_local par * I.logs_global)
    let logging_state_ref = ref true
			    
    let (init_loc, init_glob) = I.init ()
				       
    let loc_log = << ref $init_loc$ >>
    and glob_log = ref init_glob
	      
    let get_logs_names () = I.names
    let get_logs () = << unref $loc_log$ >> , !glob_log
    let set_logs (loc,glob) = <<  $loc_log$ := $loc$ >> ; glob_log := glob

    let reinit_instrumentation () = set_logs (I.reinit (get_logs()))
		       
    let start_logging () = if not !logging_state_ref then
			     (
			       set_logs(I.resume (get_logs()));
			       logging_state_ref := true
			     )
			   else
			     ()
    
    let stop_logging () = set_logs(I.pause (get_logs())); logging_state_ref := false

    let logging_state () = !logging_state_ref
									     
    (* Here we need Obj.magic to shortcut the type system on which produce 
       weak types.
       This is because BSML syntaxe transformation lead to partial applications which are then applied.
      
     *)
					     
    let sstart =
      <<
      try 
	let log = $loc_log$ in 
	fun () -> 
	try 
	  log :=( I.logging_seq_start $this$ !log )
	with exn -> 
	  output_string stderr ("sstart failure"^Printexc.to_string exn)
      with exn -> 
	(fun () -> output_string stderr  ("sstart  failure"^Printexc.to_string exn))
	>>

    and sstop = Obj.magic
		  (<< try 
		      let log = $loc_log$ in 
		      fun v ->(
			try  
		          log :=(I.logging_seq_stop $this$ v !log )
			with exn -> 
			  output_string stderr ("sstop failure"^Printexc.to_string exn)
		      )
		    with exn -> (fun v ->  output_string stderr  ("sstop  failure"^Printexc.to_string exn))
		    >>)

    and logging_out = Obj.magic
			(<< try
			  let log = $loc_log$ in 
			  fun dest v -> (
			try  
			  log :=(I.logging_comm_out $this$ dest v !log)
			with exn -> 
			  output_string stderr ("\nlogging out failure"^Printexc.to_string exn)
			  )
			  with exn -> (fun dest v ->  output_string stderr  ("\nlogging out  failure"^Printexc.to_string exn))
			  >>)

    and	logging_in = Obj.magic
		       (<< try
			 let log = $loc_log$ in 
			 fun dest v  -> (
			try  
			 log := I.logging_comm_in  $this$  dest v !log
			with exn -> 
			  output_string stderr ("\nlogging in failure"^Printexc.to_string exn)
			 )
			 with exn -> (fun dest v ->  output_string stderr  ("\nlogging in  failure"^Printexc.to_string exn))
			 >>)

    and	logging_before_sync_local = << 
				     try 
				     let log = $loc_log$ in 
				     fun () -> (
				     try  
				     log := I.logging_before_sync_local $this$ !log 
				     with exn -> 
				     output_string stderr ("\nlogging sync failure"^Printexc.to_string exn)
				     )
				     with exn -> (fun () -> output_string stderr  ("\nlogging sync local  failure"^Printexc.to_string exn))
				     >>
    and	logging_after_sync_local = << 
			      try 
			      let log = $loc_log$ in 
			      fun () -> (
				    try  
				    log := I.logging_after_sync_local $this$ !log 
				    with exn -> 
				    output_string stderr ("\nlogging sync local failure"^Printexc.to_string exn)
				    )
			      with exn -> (fun () -> output_string stderr  ("\nlogging sync local  failure"^Printexc.to_string exn))
			      >>
				     
    and logging_sync_global =
      fun () ->
      try
	let (new_loc,new_glob) =
	  I.logging_sync_global
	    ( << let log = $loc_log$ in !log >> , !glob_log )
	in
	begin
	  ignore ( << let log = $loc_log$ in  log:= $new_loc$ >>);
	  glob_log := new_glob
	end
      with exn -> output_string stderr  ("\nlogging sync global  failure"^Printexc.to_string exn)
							      
	
    let sstop : ('a -> unit) BsmlRaw.par = sstop
    and logging_in : (int-> 'a -> unit) BsmlRaw.par = logging_in
    and logging_out : (int-> 'a -> unit) BsmlRaw.par = logging_out
							 
							 
    let mkpar f = 
      if !logging_state_ref then
	begin
	  stop_logging () ;
	  let res =
	    << 
	     $sstart$ ()  ; let res = f $this$ in 
	     ( $sstop$ res;res)
	     >>
	  in
	  start_logging ();
	  res
	end
       else
	 BsmlRaw.mkpar f


    let apply vf vv =
      if !logging_state_ref then
	begin
	  stop_logging () ;
	  let res =
	    << 
	     $sstart$ (); 
	     let res = $vf$ $vv$ in 
	     begin 
              $sstop$ res;
	      res
	     end
	     >>
	  in
	  start_logging ();
	  res
	end	      
      else
	BsmlRaw.apply vf vv

    let put f =
      if !logging_state_ref then
	begin
	  stop_logging () ;
	  (* precomputing value to be sent and logging this sequential computation time
             this might double the memory consumption but it is needed to log this sequential 
             computation
	   *)
	  let outputs = 
	    << $sstart$ (); 
	     let res = Array.init bsp_p $f$  in
	     $sstop$ res; 
	     res >>
	  in
	  (* End of sequential computation *)
	  ignore (<< $logging_before_sync_local$ () >>);
	  (* logging output communication *)
	  ignore (<<  
	   Array.iteri 
	   (fun i v -> 
		   (* print_endline ("\nput_iter log out "^(string_of_int i)); *)
		   if i != $this$ then $logging_out$ i v)
	   $outputs$
	   >>);
	  (* performing the actual communication
	     we use the precomputed values
	   *)
	  let res =  BsmlRaw.put << fun i -> 
				  ( (* print_endline ("\nput_instrum "^(string_of_int i)); *)
				  Array.get $outputs$ i) >> in
	  begin
	  (* logging input communication *)
	    ignore
	      << Tools.iter_until (bsp_p -1)
	       (fun i -> 
	       (* print_endline ("\nput_iter log in "^(string_of_int i)^" bsp p "^(string_of_int bsp_p)); *)
	       if i != $this$ then  $logging_in$ i ($res$ i))
	       >>;
	  (* logging  *)
	    ignore (<< $logging_after_sync_local$ ()  >>);
	    logging_sync_global () ;
	    start_logging ();
	    res
	  end
	end
      else
	BsmlRaw.put f
	    
    let proj v =
      if !logging_state_ref then
	begin
	  stop_logging();
	  (* End of sequential computation *)
	  ignore (<< $logging_before_sync_local$ () >>);
	  ignore << iter_until (bsp_p -1)
		  (fun i -> if i != $this$ then $logging_out$ i $v$)
		  >>;
	  let res =  BsmlRaw.proj v in
	  ignore << iter_until (bsp_p -1)
		  (fun i -> if i != $this$ then $logging_in$ i (res i))
		  >>;
	  ignore (<< $logging_after_sync_local$ () >>);
	  logging_sync_global ();
	  start_logging();
	  res
	end
      else
	BsmlRaw.proj v
                     
    module Raw = BsmlRaw
  end

(** Merge build   *)

module Merge (Inner : INSTRUMENTATION) (Outter : INSTRUMENTATION) =
  functor  (Bsml:BSML)  ->
	   struct
    module InnerBsml = Inner(Bsml)
    module OutterBsml = Outter(Bsml)
			       
    type logs_local = InnerBsml.logs_local * OutterBsml.logs_local
    type logs_global = InnerBsml.logs_global * OutterBsml.logs_global

    let names = InnerBsml.names@OutterBsml.names
    let cat_strings str_arr : string =
      List.fold_left ( fun s1 s2 ->s1^" "^s2 ) "" str_arr
				  
    let init () =
      let (out_loc,out_glob) =
	try
	  OutterBsml.init ()
	with exn -> failwith ("\nlogging error in "^(cat_strings OutterBsml.names))
      and (in_loc,in_glob)   =
	try InnerBsml.init ()
	with exn -> failwith ("\nlogging error in "^(cat_strings InnerBsml.names))
      in (<< $in_loc$ , $out_loc$ >> , (in_glob, out_glob))
						     
    let reinit (in_out_loc_vector , (in_glob,out_glob) )=
      let in_loc_vect = << fst $in_out_loc_vector$ >>
      and out_loc_vect = << snd $in_out_loc_vector$ >>
      in
      let new_out_loc_vect, new_out_glob = try
	  OutterBsml.reinit (out_loc_vect, out_glob)
	with exn -> failwith ("\nlogging error in "^(cat_strings OutterBsml.names));
		    (out_loc_vect, out_glob)
      and new_in_loc_vect, new_in_glob = try
	  InnerBsml.reinit  (in_loc_vect, in_glob)
	with exn -> failwith ("\nlogging error in "^(cat_strings InnerBsml.names));
		     (in_loc_vect, in_glob)
      in
      << $new_in_loc_vect$ , $new_out_loc_vect$ >> , (new_in_glob, new_out_glob)

    let pause (loc_vector,(in_glob,out_glob)) =
      let in_loc_vect = << fst $loc_vector$ >>
      and out_loc_vect = << snd $loc_vector$ >>
      in
      let new_in_loc_vect, new_in_glob = try
	  InnerBsml.pause  (in_loc_vect, in_glob)
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		     (in_loc_vect, in_glob)
      and new_out_loc_vect, new_out_glob = try
	  OutterBsml.pause (out_loc_vect, out_glob)
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		    (out_loc_vect, out_glob)
      in
      << $new_in_loc_vect$ , $new_out_loc_vect$ >> , (new_in_glob, new_out_glob)

    let resume (loc_vector,(in_glob,out_glob)) =
      let in_loc_vect = << fst $loc_vector$ >>
      and out_loc_vect = << snd $loc_vector$ >>
      in
      let new_out_loc_vect, new_out_glob = try
	  OutterBsml.resume (out_loc_vect, out_glob)
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		    (out_loc_vect, out_glob)
      and new_in_loc_vect, new_in_glob = try
	  InnerBsml.resume  (in_loc_vect, in_glob)
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		    (in_loc_vect, in_glob)
      in
      << $new_in_loc_vect$ , $new_out_loc_vect$ >> , (new_in_glob, new_out_glob)

						    
    let logging_seq_start i (in_log_loc,out_log_loc) : logs_local =
      let new_out_log = try
	  OutterBsml.logging_seq_start i out_log_loc
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		    out_log_loc
      and new_in_log = try
	  InnerBsml.logging_seq_start i in_log_loc
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		    in_log_loc
      in (new_in_log,new_out_log)

    let logging_seq_stop i s  (in_log_loc,out_log_loc)=
      let new_in_log = try
	  InnerBsml.logging_seq_stop i s in_log_loc
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		    in_log_loc
      and new_out_log = try
	  OutterBsml.logging_seq_stop i s out_log_loc
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		    out_log_loc
      in
      (new_in_log, new_out_log)
				  
    let logging_comm_out  from dest v (in_log_loc, out_log_loc) :logs_local  =
      let new_in_log = try
	  InnerBsml.logging_comm_out from dest v in_log_loc
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		    in_log_loc
      and new_out_log = try
	  OutterBsml.logging_comm_out from dest v out_log_loc
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		    out_log_loc
      in
      (new_in_log,new_out_log)
	
    let logging_comm_in from dest v (in_log_loc, out_log_loc) :logs_local  =
      let new_out_log = try
	  OutterBsml.logging_comm_in from dest v out_log_loc
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		    out_log_loc
      and new_in_log = try
	  InnerBsml.logging_comm_in from dest v in_log_loc
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		    in_log_loc
      in
      (new_in_log,new_out_log)
	
    let logging_before_sync_local at (in_log_loc, out_log_loc)   =
      let new_in_log = try
	  InnerBsml.logging_before_sync_local at in_log_loc 
      with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		  in_log_loc
	in
    let new_out_log = try
	OutterBsml.logging_before_sync_local at out_log_loc
      with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		  out_log_loc
    in
    (new_in_log, new_out_log)
	
    let logging_after_sync_local at (in_log_loc, out_log_loc)   =
      let new_in_log = try
	  InnerBsml.logging_after_sync_local at in_log_loc 
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		    in_log_loc
      in
      let new_out_log = try
	  OutterBsml.logging_after_sync_local at out_log_loc 
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		    out_log_loc
      in
      (new_in_log, new_out_log)

	
    let logging_sync_global  (in_out_loc_vector , (in_glob,out_glob) ) =
      let in_loc_vect = << fst $in_out_loc_vector$ >>
      and out_loc_vect = << snd $in_out_loc_vector$ >>
      in
      let new_out_log_loc,new_out_log_glob = try 
	  OutterBsml.logging_sync_global (out_loc_vect,out_glob)
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings OutterBsml.names));
		    (out_loc_vect,out_glob)
      in
      let new_in_log_loc, new_in_log_glob = try 
	  InnerBsml.logging_sync_global (in_loc_vect,in_glob)
	with exn -> output_string stderr ("\nlogging error in "^(cat_strings InnerBsml.names));
		    (in_loc_vect,in_glob)
      in
      (<< $new_in_log_loc$,$new_out_log_loc$>>, (new_in_log_glob, new_out_log_glob))
    module Raw = Bsml
  end

module COERCE (Bs : BSMLSEQ) (B : INSTRUMENTED_BSML with type 'a par = 'a Bsmlsig.seqpar and module Raw = Bs) :  INSTRUMENTED_BSMLSEQ = B  
