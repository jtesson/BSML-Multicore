(*********************************************************************)
let natmod i m = (m+(i mod m)) mod m

let from_to n1 n2 = 
  let rec aux list n2 = 
    if(n1>n2) then list else aux (n2::list) (n2-1) in
    aux [] n2

let rec filtermap p f = function 
    [] -> []
  | h::t -> 
      if p h 
      then (f h)::(filtermap p f t)
      else filtermap p f t 

let id x = x

let compose f g x = f (g x)

let is_empty v = 
  Marshal.to_string v [Marshal.Closures]=
  Marshal.to_string None [Marshal.Closures]

let unref r = !r

let size_of_data d =
  if is_empty d then 0 else String.length( Marshal.to_string d [Marshal.Closures])

let iter_until i f =
  let rec aux i j f =
    begin
      (* print_int i; *)
      f i ; if i < j then aux (i+1) j f
    end
  in aux 0 i f

let string_of_matrix st_of_data mat =
  Array.fold_left
    (fun s arr ->
     (Array.fold_left
       (fun s d -> s^"\t"^(st_of_data d) )
       s
       arr
     )^"\n"
    )
    ""
    mat
(*********************************************************************)
