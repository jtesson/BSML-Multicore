open String

type param = { p : int ; g : float ; l : float}

let processline s = 
  let len = length s 
  and l = rindex s ',' in
  let ls = sub s (l+1) (len-l-1) in
  let g = index s ',' in
  let s''= sub s 0 g 
  and gs = sub s (g+1) (l-g-1) in
  {p=(int_of_string s'');g=(float_of_string gs);l=(float_of_string ls)}

let readbsmllibrc p =
  let chin = 
    try 
      open_in ((Sys.getenv "HOME")^"/.bsmllibrc") 
    with _ -> (print_string "~/.bsmllibrc unavailable.\n";flush stdout; exit 1) in
  try
    let parameters = ref (processline (input_line chin)) in
    if p=None 
    then !parameters
    else let Some bspp = p in
    while !parameters.p<>bspp do
      parameters:=processline (input_line chin)
    done;!parameters
  with _ -> close_in chin;{p=4;g=1.;l=10.}
