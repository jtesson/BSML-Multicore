(** Useful sequential functions *)

(** Modulo *)
val natmod: int -> int -> int

(** [from_to n1 n2] = [[n1;n1+1;...;n2]] *)
val from_to: int -> int -> int list

(** [filtermap p f l] applies [f] to each element of [l] which
satifies the predicate [p] *)
val filtermap: ('a -> bool) -> ('a -> 'b) -> 'a list -> 'b list

(** Function composition. *)
val compose: ('a -> 'b) -> ('c -> 'a) -> 'c -> 'b

(** Identity function *)
val id: 'a -> 'a

(** Tests whether a value is considered as an empty message. *)
val is_empty: 'a -> bool

(** pointer dereferencing writtent as a functional application  *)
val unref: 'a ref -> 'a

(** computing the amount of byte BSML will transfer for a given data  *)
val size_of_data : 'a -> int

(** apply a function [f] to all int from 0 to [i]   *)
val iter_until : int -> (int -> 'a) -> unit

(** string representation of a matrix  *)
val string_of_matrix : ('a -> string) -> 'a array array -> string
