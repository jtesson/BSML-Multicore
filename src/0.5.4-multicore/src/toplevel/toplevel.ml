let init_output () =
  Toploop.print_out_value := Bsml.print_out_value
let init_msg () =
  begin
    print_string "        Bulk Synchronous Parallel ML version 0.5.4";
    print_newline();
    print_string "          The BSP Machine has ";
    print_int Bsml.bsp_p;
    print_string " processor";
    if (Bsml.bsp_p>1) then print_string "s";
    print_newline();
    print_string "          o BSP parameters g = ";
    print_float  Bsml.bsp_g;
    print_string " flops/word";
    print_newline();
    print_string "          o BSP parameters L = ";
    print_float  Bsml.bsp_l;
    print_string " flops";
    print_newline();
    print_string "          o BSP parameters r = ";
    print_float  Bsml.bsp_r;
    print_string " flops/s";
    print_newline();
  end

let _ = init_output () ; init_msg ()
