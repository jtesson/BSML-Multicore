module Make = functor (P:Bsmlsig.MACHINE_PARAMETERS) ->
struct

  (* Exceptions *)

  exception Invalid_processor of int

  exception Timer_failure of string

  exception Parmatch_nextcase
  exception Parmatch_failure

  let parameters = 
    begin 
      P.read 0; 
      (* Suppose que (read 0) renvoie les paramètres mpm dans la première
	 entrée du fichier de configuration *)
      P.get ()
    end

  let bsp_p = parameters.P.p
  let bsp_g = parameters.P.g 
  and bsp_l = parameters.P.l
  let bsp_r = parameters.P.r

  let _ = if bsp_p < 1 then failwith ("BSML require at least 1 processor. Unable to start with "^(string_of_int bsp_p)^" processor") else ()
																	    
  let bsp_time_start = ref 0.

  (* val with_bounds : int -> bool = <fun> *)
  let within_bounds i = (0 <= i) && (i < bsp_p)
                                      
  open Bsmlsig                                       
  type 'a par = 'a seqpar

  let mkpar f = 
    BsmlSequentialPar(Array.init bsp_p f)

  let get (BsmlSequentialPar vv) (BsmlSequentialPar vi) =
    BsmlSequentialPar(Array.init bsp_p 
				 (fun j->
				    if within_bounds j
				    then vv.(vi.(j))
				    else raise (Invalid_processor j)))

  let put (BsmlSequentialPar vf) = 
    BsmlSequentialPar(Array.init bsp_p (fun i -> fun j -> (vf.(j)) i))

  let apply (BsmlSequentialPar vf) (BsmlSequentialPar vv) =
    BsmlSequentialPar (Array.init bsp_p (fun i-> (vf.(i)) (vv.(i))))

  let proj (BsmlSequentialPar v) n =
    if not (within_bounds n)
    then raise (Invalid_processor n)
    else v.(n)

  let argv = Sys.argv

  type timing_state = Running | Stopped

  let timing = ref Stopped

  let start_timing () = 
    if !timing=Stopped
    then 
      (bsp_time_start := Sys.time();
       timing := Running)
    else
      raise (Timer_failure "Timer is already running")

  let stop_timing () =
    if !timing=Running
    then 
      (bsp_time_start := (Sys.time()) -. (!bsp_time_start);
       timing:=Stopped)
    else
      raise (Timer_failure "Timer was not started!")

  let get_cost () = 
    mkpar(fun pid -> !bsp_time_start)

  exception Error of string

  let abort i s = 
    print_string s;
    flush stdout;
    exit i

  (* For Pretty-Printing *)

  open Format
  open Outcometree

  exception Ellipsis
  let cautious f ppf arg =
    try f ppf arg with
	Ellipsis -> fprintf ppf "..."

  let rec print_ident ppf =
    function
	Oide_ident s -> fprintf ppf "%s" s
      | Oide_dot (id, s) -> fprintf ppf "%a.%s" print_ident id s
      | Oide_apply (id1, id2) ->
	  fprintf ppf "%a(%a)" print_ident id1 print_ident id2
	    
  let print_out_value ?(typename="BsmlSequentialPar") ppf tree =
    let rec print_tree ppf =
      function
	| Oval_constr (Oide_ident typename, 
		       [Oval_array tl]) ->
            fprintf ppf "@[<2><%a>@]" (print_tree_list print_tree ",") tl
	| Oval_constr (Oide_dot (_ , "BsmlSequentialPar"),
		       [Oval_array tl]) ->
            fprintf ppf "@[<2><%a>@]" (print_tree_list print_tree ",") tl
	| Oval_constr (name, (_ :: _ as params)) ->
            fprintf ppf "@[<1>%a@ %a@]" print_ident name
              (print_tree_list print_simple_tree "") params
	| Oval_variant (name, Some param) ->
            fprintf ppf "@[<2>`%s@ %a@]" name print_simple_tree param
	| tree -> print_simple_tree ppf tree
    and print_simple_tree ppf =
      function
	  Oval_int i -> fprintf ppf "%i" i
	| Oval_int32 i -> fprintf ppf "%ldl" i
	| Oval_int64 i -> fprintf ppf "%LdL" i
	| Oval_nativeint i -> fprintf ppf "%ndn" i
	| Oval_float f -> fprintf ppf "%.12g" f
	| Oval_char c -> fprintf ppf "'%s'" (Char.escaped c)
	| Oval_string s ->
            begin try fprintf ppf "\"%s\"" (String.escaped s) with
		Invalid_argument "String.create" -> fprintf ppf "<huge string>"
            end
	| Oval_list tl ->
            fprintf ppf "@[<1>[%a]@]" (print_tree_list print_tree ";") tl
	| Oval_array tl ->
            fprintf ppf "@[<2>[|%a|]@]" (print_tree_list print_tree ";") tl
	| Oval_constr (Oide_ident "true", []) -> fprintf ppf "true"
	| Oval_constr (Oide_ident "false", []) -> fprintf ppf "false"
	| Oval_constr (name, []) -> print_ident ppf name
	| Oval_variant (name, None) -> fprintf ppf "`%s" name
	| Oval_stuff s -> fprintf ppf "%s" s
	| Oval_record fel ->
            fprintf ppf "@[<1>{%a}@]" (cautious (print_fields true)) fel
	| Oval_tuple tree_list ->
            fprintf ppf "@[(%a)@]" (print_tree_list print_tree ",") tree_list
	| Oval_ellipsis -> raise Ellipsis
	| Oval_printer f -> f ppf
	| tree -> fprintf ppf "@[<1>(%a)@]" (cautious print_tree) tree
    and print_fields first ppf =
      function
	  [] -> ()
	| (name, tree) :: fields ->
            if not first then fprintf ppf ";@ ";
            fprintf ppf "@[<1>%a = @,%a@]" print_ident name (cautious print_tree)
              tree;
            print_fields false ppf fields
    and print_tree_list print_item sep ppf tree_list =
      let rec print_list first ppf =
	function
            [] -> ()
	  | tree :: tree_list ->
              if not first then fprintf ppf "%s@ " sep;
              print_item ppf tree;
              print_list false ppf tree_list
      in
	cautious (print_list true) ppf tree_list
    in
      cautious print_tree ppf tree
	
end
