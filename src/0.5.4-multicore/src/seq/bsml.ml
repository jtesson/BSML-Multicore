(**********************************************************)
(*  BSML library : Sequential simulator			  *)
(*  September 2008	          	                  *)
(*  Author: F. Loulergue				  *)
(**********************************************************)

include Sequential.Make(Parameters_in_file)
