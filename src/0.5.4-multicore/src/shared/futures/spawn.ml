(* 

Note : to ensure proper garbage collection, worker should have their head of the queue and main thread only the tail, 
so that any action  performed by all worker will not be accessible by anyone.

 *)
module Make : 
functor (P : Bsmlsig.MACHINE_PARAMETERS) ->
Bsmlsig.BSML
=
  functor (Parameters : Bsmlsig.MACHINE_PARAMETERS) -> 
  struct

    let argv = Sys.argv
             
    (*  getting BSP parameters *)
    let parameters = 
      begin
        Parameters.read 0;
        Parameters.get()
      end
      
    let bsp_p = parameters.Parameters.p
    let bsp_g = parameters.Parameters.g
    let bsp_l = parameters.Parameters.l
    let bsp_r = parameters.Parameters.r
              
    (** A parallel vector is an array of future of size bsp_p. 
        Initialized by launching p threads.
     *)
    type 'a par = 'a Domain.t array
                
    let mkpar (f : int -> 'a) : 'a par = Array.init bsp_p (fun i -> Domain.spawn (fun () -> f i ))
      
    let apply (vf: ('a -> 'b) par) (vv: 'a par) : 'b par = Array.init bsp_p  (fun i -> Domain.spawn (fun () -> Domain.join vf.(i)  (Domain.join vv.(i)))) 
      
    let proj (v : 'a par) : int -> 'a =
      let arr = Array.map Domain.join v in
      Array.get arr

    let put (v : (int -> 'a) par) : (int -> 'a) par =
      let arr = Array.map  Domain.join v in
      Array.init bsp_p (fun i -> Domain.spawn(fun () -> fun j -> arr.(j) i))
      
    exception Invalid_processor of int
                                 
    let within_bounds n = (0<=n) && (n<bsp_p)

      
    exception Timer_failure of string
                             
    (* exception Parmatch_nextcase *)
    (* exception Parmatch_failure *)

    type timing_state = Running | Stopped

    let bsp_time_start = ref 0.

    let timing = ref Stopped

    let start_timing () =
      if !timing=Stopped
      then
        (bsp_time_start := Unix.time();
         timing := Running)
      else
        raise (Timer_failure "Timer is already running")

    let stop_timing () =
      if !timing=Running
      then
        (bsp_time_start := (Unix.time()) -. (!bsp_time_start);
         timing:=Stopped)
      else
        raise (Timer_failure "Timer was not started!")

    let get_cost () = Array.make bsp_p (Domain.spawn (fun () -> !bsp_time_start))

    let abort error_code s = 
      print_string s;flush stdout;
      exit error_code
end
