(* 

Note : to ensure proper garbage collection, worker should have their head of the queue and main thread only the tail, 
so that any action  performed by all worker will not be accessible by anyone.

 *)
open SharedQueue
module Make : 
functor (P : Bsmlsig.MACHINE_PARAMETERS) ->
Bsmlsig.BSML
=
  functor (Parameters : Bsmlsig.MACHINE_PARAMETERS) -> 
  struct

    let argv = Sys.argv
             
    (*  getting BSP parameters *)
    let parameters = 
      begin
        Parameters.read 0;
        Parameters.get()
      end
      
    let bsp_p = parameters.Parameters.p
    let bsp_g = parameters.Parameters.g
    let bsp_l = parameters.Parameters.l
    let bsp_r = parameters.Parameters.r
              
    (** A parallel vector is an array of future of size bsp_p. 
        Initially None, the value at position [i] will eventually be update to [Some a] value by [i]th thread.
        Position [i] of a vector can only be written by thread [i]
     *)
    type 'a par = 'a option array
                
    (** actions to be stored in a global queue, waiting to be executed by each threads.
      Each primitive can either ask the updating of a vector by all threads, or ask all threads to stop ([sync]) *)
    type action =
      | Mk :  (int -> 'a) * 'a par -> action
      | App : ('a -> 'b ) par * 'a par * 'b par -> action
      | Put :  (int -> 'a) par * ('a array) par  -> action
      | Sync : action
      
      
    (** Preparing a new vector   *)
    let make_par p = Array.make p None
    let make_par_some p f = Array.init p (fun i -> Some (f i))

    let deep_copy v = v
                   
    let no_some o = match o with None -> failwith "no_some None" | Some s -> s  
                                                                           
    (** Local evaluation of action [act] by thread [i].
      All previous actions where performed by thread [i], so for any known vector, the value at position [i] is determined to [Some] value.
     *)
    let eval act i =
      begin
        match act with
        (* mkpar action : evaluate [f i] and store the result at pos [i] of the given vector *)
        | Mk (f, a) -> a.(i) <- Some (f i); true
        (* apply action : evaluate [vf.(i) vv.(i)] and store the result at pos [i] of the given vector. *)
        | App (vf, vv, vr) -> 
           begin
             match vf.(i), vv.(i) with
             | Some f, Some v -> vr.(i) <- Some (f v)
             | _ -> assert false
           end; true
        (* Sync cause the current evaluator thread to stop. Used for synchronisation at proj and put. *)
        | Sync ->false
        (* Before synchronisation, the thread has to compute values for other threads.
           Computation has to take place locally as it could perform side effects on values held locally.
         *)
        | Put (vf,vr)  -> let res = Array.init bsp_p (fun dest -> ((no_some (vf.(i)))dest)) in
                          vr.(i) <- Some (deep_copy res)
                          ; false
      end
    (** A worker is a loop that evaluate concurrently all code meant to be locally executed.
      It stop at first synchronisation. An other worker will be launched after that.
     *)
    (* Initialisation of the action queue and the threads *)
    let actions : action queue =  (empty ())                               

     let worker pid =
      let cont = ref true in 
      let my_queue = ref actions in
      while !cont do
        while (is_empty !my_queue )do
          Domain.Sync.critical_section (fun () ->
              if (is_empty !my_queue ) then
                Domain.Sync.wait ()
            )
        done;
        let act = head !my_queue in
        cont := eval act pid;
        my_queue:= pop !my_queue
      done

    let threads = Array.init bsp_p (fun i -> Domain.spawn (fun () -> worker i))
    let threads_ids = Array.map (fun th -> Domain.get_id th) threads
                    
    (* Notification of all active threads *)
    let notify_all ()  = ignore (Array.map (fun th -> Domain.Sync.notify th)  threads_ids)
    (* Synchronisation with all active threads *)
    let join_all ()  =  ignore (Array.map (fun th -> Domain.join th)  threads)
    (* Replacement of all threads by fresh one. (olders should have been stopped at this point) *)
    let start_all ()  =  ignore(Array.iteri
                                  (fun i th ->
                                    let nth = Domain.spawn (fun () -> worker  i) in
                                    threads.(i) <- nth ;
                                    threads_ids.(i) <- Domain.get_id nth 
                                  )
                                  threads
                               )

    let add act = enqueue actions act
                
    let mkpar f =
      let v = make_par bsp_p in
      add (Mk (f, v));notify_all(); v
      
    let apply vf vv =
      let v = make_par bsp_p in
      add (App (vf, vv,v)); notify_all();v
      
    let proj v =
      add (Sync);
      notify_all ();
      join_all () ;
      throw_away actions;
      assert (is_empty actions);
      start_all ();
      (fun i -> no_some v.(i))

    let put (v : (int -> 'a) par)  =
      let vr = make_par bsp_p in
      add (Put (v,vr));
      notify_all ();
      throw_away actions;
      assert (is_empty actions);
      join_all () ;
      start_all () ;
      make_par_some bsp_p (fun dest from ->  (no_some vr.(from)).(dest))

    exception Invalid_processor of int
                                 
    let within_bounds n = (0<=n) && (n<bsp_p)

      
    exception Timer_failure of string
                             
    (* exception Parmatch_nextcase *)
    (* exception Parmatch_failure *)

    type timing_state = Running | Stopped

    let bsp_time_start = ref 0.

    let timing = ref Stopped

    let start_timing () =
      if !timing=Stopped
      then
        (bsp_time_start := Unix.time();
         timing := Running)
      else
        raise (Timer_failure "Timer is already running")

    let stop_timing () =
      if !timing=Running
      then
        (bsp_time_start := (Unix.time()) -. (!bsp_time_start);
         timing:=Stopped)
      else
        raise (Timer_failure "Timer was not started!")

    let get_cost () = Array.make bsp_p (Some !bsp_time_start)

    let abort error_code s = 
      print_string s;flush stdout;
      exit error_code
end
