type 'a cell =
  Empty
| Cell of 'a* 'a cell ref


type 'a queue  = 'a cell ref *'a cell ref

let empty () : 'a queue  = ref (Empty),ref (Empty)

let enqueue (first, last)  v  =
  let c = Cell (v,  ref (Empty )) in 
  match !last with
    Empty ->  first := c; last := c
  | Cell (v, next)  -> next := c; last :=c

                    
let head_cell  ((first, last) : 'a queue)   = !first


let is_empty (first, last : 'a queue) =
  match !first with
    Empty ->  true
  | _ -> false


let pop (first, last : 'a queue) : 'a queue =
  match !first with
    Empty ->  failwith "empty queue"
  | Cell (a, b) ->  (b,last)

let has_tail (first, last : 'a queue) =
  match !first with
    Empty ->  false
  | Cell (a, b) ->
     match !b with
       Empty -> false
     | _ -> true

          
let head  (first, last  : 'a queue)   =
  match !first with
    Empty ->  failwith "empty queue"
  | Cell (a, b) -> a


let tail (first, last : 'a queue) : 'a queue =
  match !first with
    Empty ->  failwith "empty queue"
  | Cell (a, b) ->
     if
       !b = Empty then failwith "empty queue"
     else
       (b,last)

let rec length (first, last : 'a queue) : int  =
  match !first with
  | Empty -> 0
  | Cell (a, b) -> 1 + length (b,last)
    
let throw_away (first, last : 'a queue) : unit  =
  first := Empty; last := Empty

module Test (U : sig val u : unit end)= struct
  
  let test : int queue = empty ()
  let _ = assert (not (has_tail test)) ;assert (length test = 0) 
  let _ = enqueue  test 0
  let _ = assert (not (has_tail test)); assert (length test = 1) ; assert (head test = 0)     
  let _ = enqueue  test 1; test
  let _ = assert (length test = 2) ;assert ((has_tail test));assert (head test = 0)     
  let _ = enqueue  test 2;enqueue  test 3; test
  let _ = assert (length test = 4) ;assert ((has_tail test));assert (head test = 0)     

  let _ = head_cell  test

  let _ = let t = ref test and
              l = length test in
          for i = 1 to 2 do
            print_int i;
            assert (has_tail !t);
            t := tail !t;
            assert (length !t = l-i);
          done; 
          t := tail !t;
          assert (length !t = 1);
          t

  let _ =  tail test
  let _ =  throw_away test ; assert (is_empty test)
  let _ = enqueue  test 0;enqueue  test 1;enqueue  test 2
  let _ = assert (not (is_empty test)); assert (length test = 3) ; assert (head test = 0)     
  let _ =  throw_away test ; assert (is_empty test)
  let _ = assert ((is_empty test)); assert (length test = 0)

end       

(* module T = Test (struct let u = () end)                                       *)
