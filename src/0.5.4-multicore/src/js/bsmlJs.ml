(**********************************************************)
(*  BSML library : full BSML environment constructor	  *)
(*  February 2016	          	                  *)
(*  Author: J. Tesson     				  *)
(**********************************************************)
open Bsmlsig

module Make  (P:MACHINE_PARAMETERS)
             (Instrum : functor (B:BSMLSEQ) -> ( INSTRUMENTED_BSMLSEQ with type 'a par = 'a B.par)) 
=
  struct
    module BsmlRaw = Sequential.Make(P)
    module Bsml = Instrum (BsmlRaw)
    module Stdlib = struct
      module Base = Bsmlbase.Make(Bsml)
      module Comm = Bsmlcomm.Make(Bsml)
      module Sort = Bsmlsort.Make(Bsml)
      module Back = Bsmlbckcomp.Make(Bsml)
    end
  end

