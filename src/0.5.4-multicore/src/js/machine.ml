		   
module type Proc = sig  val p : int end
module type MachineParam = sig
    val p : int
    val g : float
    val r : float
    val l : float
  end

module Dummy_Machine =
  functor (P:Proc) ->
	  struct
	    type bsp = { p : int ; g : float ; l : float; r : float }
	    let parameters = ref {p=P.p;g=18.;l=72000.;r=5000.}
				 
	    let read p =
	      let theparameters = ref {p=P.p;g=18.;l=72000.;r=5000.}
	      in
	      if p=0 
	      then ()
	      else
		parameters:=!theparameters
			  
	    let get () = !parameters
	  end 

module Make_Machine =
functor (P:MachineParam) ->
    struct
      type bsp = { p : int ; g : float ; l : float; r : float }
      let parameters = ref {p=P.p;g=P.g;l=P.l;r=P.r}
		
      let read p =
	let theparameters = ref {p=P.p;g=P.g;l=P.l;r=P.r}
	in
	if p=0 
	then ()
	else
	  parameters:=!theparameters
		    
      let get () = !parameters
    end 
      
		       
