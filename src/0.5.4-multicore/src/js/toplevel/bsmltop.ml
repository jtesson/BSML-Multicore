open Bsmlsig

open BsmlInstrumentation

module Mktop  (P : Machine.MachineParam)
       (Instrum : functor (B:BSMLSEQ) -> (INSTRUMENTED_BSMLSEQ with type 'a par = 'a B.par))
       =
struct
  module BspMachine = Machine.Make_Machine(P)
  
  include BsmlJs.Make (BspMachine) (Instrum)
  let init_output () =
    Toploop.print_out_value := BsmlRaw.print_out_value 
  let init_msg () =
    begin
      print_string "        Bulk Synchronous Parallel ML version 0.5.4";
      print_newline();
      print_string "          The BSP Machine has ";
      print_int Bsml.bsp_p;
      print_string " processor";
      if (Bsml.bsp_p>1) then print_string "s";
      print_newline();
      print_string "          o BSP parameter g = ";
      print_float  Bsml.bsp_g;
      print_string " flops/word";
      print_newline();
      print_string "          o BSP parameter L = ";
      print_float  Bsml.bsp_l;
      print_string " flops";
      print_newline();
      print_string "          o BSP parameter r = ";
      print_float  Bsml.bsp_r;
      print_string " flops/s";
      print_newline();
    end
end
