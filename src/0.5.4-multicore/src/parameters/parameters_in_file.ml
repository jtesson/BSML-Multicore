open String

type bsp = { p : int ; g : float ; l : float; r : float }

let parameters = ref {p=16;g=18.;l=72000.;r=5000.}

let list_from_string s separator  = 
  let rec aux (s,l) = 
    if not(contains s separator)
    then ("",s::l)
    else
      let i = index s separator in
      let new_s = sub s (i+1) (length s - 1 -i) 
      and element = sub s 0 i in
	aux (new_s,element::l) in
    List.rev(snd(aux (s,[])))

let read p =
  let processline s = 
    let ps::l = list_from_string s ',' in 
    let [g;l;r] = List.map float_of_string l in
      {p=(int_of_string ps);
       g=g; l=l; r=r;} in
  let chin = 
    try 
      open_in ((Sys.getenv "HOME")^"/.bsmlrc") 
    with _ -> 
      (print_string "~/.bsmlrc unavailable.\n";flush stdout; exit 1) in
  let theparameters = ref(processline (input_line chin)) in
    try
      if p=0 
      then parameters := !theparameters
      else
	begin
	  while !theparameters.p<>p do
	    theparameters:=processline (input_line chin)
	  done;
	  parameters:=!theparameters;
	end
    with _ -> 
      begin
	(if (!theparameters.p<>p)
	 then parameters:={p=p;g=0.;l=0.;r=0.}
	 else parameters:=!theparameters);
	close_in chin
      end
      
let get () = !parameters
