exception Tcpip_error of string
  
(* The parameters are organized as follows as arguments of the command line
   when the BSML program is run using bsmlrun: 
   - At index 1: the number of processes
   - At index 2: the pid of the current process
   - At index 3: the port used by the current process
   - At index 4: the host name *)
  
(* Read the number of processes on the arguments *)
let p_ref = ref (int_of_string Sys.argv.(1))
let nprocs () = !p_ref
  
(* Read the pid on the arguments *)
let pid_ref = ref (int_of_string Sys.argv.(2))
let pid () = !pid_ref

(* Port used for communications *)
let port = int_of_string Sys.argv.(3)

(* ########################################################################## *)
(* ############################ Constants ################################### *)
(* ########################################################################## *)

(* Several tries are allowed to establish the connection with other
   processes *)
let number_of_tries_connect = 3000
let delay_between_tries_connect = 0.1

(* print an error message *)
let print_err_pid s = 
  prerr_string ("Process "^(string_of_int !pid_ref)^": "^s^"\n");
  flush stderr

(* print a message and raise an exception *)
let error_tcpip s =
  print_err_pid s;
  raise (Tcpip_error s)  

    
(* IP Address obtained from the process's name *)
let ipaddr = 
  try
    Array.init (!p_ref) 
      (fun i->(Unix.gethostbyname(Sys.argv.(i+4))).Unix.h_addr_list.(0))
  with _ -> error_tcpip ("Unable to have the IP of the machines")
    
(* PID of the processes, to send them values (taken from latin
   square) *)
let tab_pid_to_send =
  Array.init (!p_ref-1) (fun i -> ((!pid_ref)+i+1) mod (!p_ref))

(* PID of the processes, to received from them values *)
let tab_pid_to_receveid = 
  Array.of_list (List.rev (Array.to_list tab_pid_to_send))

(* the socket for accepted values *)
let the_socket = ref []
  
(* Arrays of the communicated channel from another process *)
let tab_of_receveid_connection = Array.make (!p_ref) stdin
let tab_of_sended_connection   = Array.make (!p_ref) stdout

(* List of the sockets for sended values *)
let list_of_sended_socket = ref []
  
(* condition and mutex to join the principal thread and another
   thread *)
let not_join = ref true
and mutex_join = Mutex.create() 
and cond_join = Condition.create()
and thread_has_not_well_work = ref true

(* function to join the principal thread *)
let join_principal () =
  Mutex.lock mutex_join;
  not_join:=false;
  Condition.signal cond_join;
  Mutex.unlock mutex_join

(* function to wait the auxiliary threads, called by the principal
   thread *)
let wait_auxiliary () =
  Mutex.lock mutex_join;
  while !not_join do
    Condition.wait cond_join mutex_join
  done;
  Mutex.unlock mutex_join


(* ########################################################################## *)
(* ############################ Send ######################################## *)
(* ########################################################################## *)

let value_of_size_zero = Marshal.to_string None [Marshal.Closures]

let send_value vv pid_for = 
  (try
     let chout = tab_of_sended_connection.(pid_for) in
     let msg = Marshal.to_string vv [Marshal.Closures] in
       if msg=value_of_size_zero
       then (Marshal.to_channel chout 0 [];flush chout)
       else (let size = String.length msg in
               Marshal.to_channel chout size [];
               flush chout;
               output chout msg 0 size;
               flush chout);
       thread_has_not_well_work:=false
   with _ -> ());
  join_principal ()

let send data = 
  (* create the buffered array of string *)
  let buffer = Array.make (!p_ref) "" in
    (* for all another process *)
    for i=(!p_ref-2) downto 0 do
      (* calculate the pid of the destination *)
      let pid_for  = tab_pid_to_send.(i) 
      and pid_from = tab_pid_to_receveid.(i) in
      let inchan   = tab_of_receveid_connection.(pid_from) in
	thread_has_not_well_work:=true;
	not_join:=true;
	(* sended the value *)
	ignore (Thread.create (send_value data.(pid_for)) pid_for);
	(* received a value *)
	(let size = (Marshal.from_channel inchan) in 
           if (size>0) then 
	     let buf = String.create size in
	       really_input inchan buf 0 size;
	       buffer.(pid_from)<-buf);
	wait_auxiliary (); 
	if !thread_has_not_well_work 
	then error_tcpip 
	  ("Unable to send a value to: "^(string_of_int pid_for))
    done;
    (* transform the receveid string to values *)
    (* the process do not send a value to itself ;-) *)
    let result = 
      Array.make (!p_ref) 
	((Marshal.from_string value_of_size_zero 0):'a) in
      for i=(!p_ref-1) downto 0 do
	if ((String.length buffer.(i))>0)&&(i<>(!pid_ref)) then
	  result.(i)<-((Marshal.from_string buffer.(i) 0):'a)
      done;
      result.(!pid_ref)<-data.(!pid_ref);
      (* and return the result *)
      result

(* ########################################################################## *)
(* ############################ Initialize ################################## *)
(* ########################################################################## *)

let initialize_accept_comm () = 
  let nb_connect = ref ((!p_ref)-1) in
    (try
       while (!nb_connect>0) do
	 (* accept a connection *)
	 let (s_descr,s_addr) = Unix.accept (List.hd !the_socket) in
	 let inchan=Unix.in_channel_of_descr s_descr in
	   (* read the pid of the accepted process *)
	 let pid_from = input_value inchan in
	   (* save the channel to read later the values of the process*)
	   tab_of_receveid_connection.(pid_from)<-inchan;
	   decr nb_connect
       done;
       thread_has_not_well_work:=false;
     with _ -> ());
    join_principal ()

(* function to connect to a distant processes *)
let connect to_pid =
  (* Opening connection *)
  let s_addr = (Unix.ADDR_INET(ipaddr.(to_pid), port-(!pid_ref)+to_pid))
  and not_success = ref true and the_channels=ref (stdout) and tries = ref 0 in
  let sock = try
    Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0
  with _ -> error_tcpip "Unable to create a socket for connection" in
    while (!not_success)&&(!tries<number_of_tries_connect) do
      try
	Unix.connect sock s_addr;
	not_success:=false
      with _ -> (Thread.delay delay_between_tries_connect;tries:=!tries+1)
    done;
    if (!not_success) 
    then error_tcpip ("Unable to connect to:"^(string_of_int to_pid));
    the_channels:=Unix.out_channel_of_descr sock;
    output_value !the_channels (!pid_ref);
    flush !the_channels;
    list_of_sended_socket:=sock::(!list_of_sended_socket);
    !the_channels

let initialize_connect () =
  for i=(!p_ref-2) downto 0 do
    (* first connect to the other processes *)
    let to_pid = tab_pid_to_send.(i) in
      (* and save the channel *)
      tab_of_sended_connection.(to_pid) <- connect to_pid;
  done
    
let initialize argv =
  (* print_pid "BEGIN INITIALIZE"; *)
  let sock = 
    try
      Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0
    with _ -> error_tcpip "Unable to create a socket for acceptation" in
  let not_success = ref true and tries = ref 0 in
    while (!not_success)&&(!tries<number_of_tries_connect) do
      try
	Unix.bind sock (Unix.ADDR_INET(ipaddr.(!pid_ref), port));
	not_success:=false;
      with _ -> (Thread.delay delay_between_tries_connect;tries:=!tries+1)
    done;
    if (!not_success) 
    then error_tcpip "Unable to bind the socket at initialization";
    Unix.listen sock (((!p_ref)-1));
    the_socket:=[sock];
    thread_has_not_well_work:=true;
    not_join:=true;
    ignore (Thread.create initialize_accept_comm ());
    initialize_connect ();
    wait_auxiliary ();
    if !thread_has_not_well_work 
    then error_tcpip ("Unable to connect to another process")

(* ########################################################################## *)
(* ############################ Finalize #################################### *)
(* ########################################################################## *)

let finalize () = 
  (try
     Unix.close (List.hd !the_socket);
     ignore (List.map Unix.close !list_of_sended_socket)
   with _ -> ())

(* ########################################################################## *)
(* ############################ Others ###################################### *)
(* ########################################################################## *)

let args = ref (let n=((!p_ref)+4) in 
                  Array.append [|Sys.argv.(0)|]
                    (Array.sub Sys.argv n ((Array.length Sys.argv)-n)))

let argv () = !args
  
let wtime = Unix.gettimeofday

let abort error_code =  exit error_code
