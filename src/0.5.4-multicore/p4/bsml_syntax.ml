(*pp camlp4of *)
(*
  Louis Gesbert / BSML
*)

open Camlp4
open Camlp4.PreCast

module Make (Syntax : Sig.Camlp4Syntax) = struct
  open Sig
  include Syntax

  let rec build_fun _loc e idlist = match idlist with
    | [] -> e
    | id::idr -> <:expr< fun $lid:id$ -> $build_fun _loc e idr$ >>

  let rec build_apply _loc e explist = match explist with
    | [] -> e
    | ex::exr -> <:expr< Bsml.apply $build_apply _loc e exr$ $ex$ >>

  let quot_level = ref 0
  let locvar_list = ref []
  let fresh = let r = ref 10 in fun _loc () -> incr r; "x" ^ string_of_int !r
  let _ = Camlp4_config.antiquotations := true

  EXTEND Gram
    GLOBAL: expr;

    expr: LEVEL "simple"
      [ [ `ANTIQUOT((""|"expr"), a) ->
            if !quot_level = 0 then failwith "$expr$ outside of << >>";
            match Gram.parse_string expr _loc a with
            | <:expr< $lid:id$ >> ->
              let nid = "__Li_"^id in
                (if not (List.mem_assoc nid !locvar_list)
                 then locvar_list := (nid, <:expr< $lid:id$ >>) :: !locvar_list);
                <:expr< $lid:nid$ >>
            | <:expr< $e$ >> ->
              let id = "__Le_"^(fresh _loc ()) in
                assert (not (List.mem_assoc id !locvar_list));
                locvar_list := (id, <:expr< $e$ >>) :: !locvar_list;
                <:expr< $lid:id$ >>
      ]
    ];

  END

  let expand_vector_quot_expr _loc _loc_name_opt quotation_contents =
    let old_locvar_list = !locvar_list in
      locvar_list := []; incr quot_level;
      let e = Gram.parse_string expr _loc quotation_contents in
      let idlist,exlist = List.split (List.remove_assoc "__Li_this" !locvar_list) in
      let apfun = build_fun _loc e idlist in
        locvar_list := old_locvar_list; decr quot_level;
        build_apply _loc <:expr< Bsml.mkpar ( fun __Li_this -> $apfun$ ) >> (List.rev exlist)

  let expand_vector_quot_str_item _loc _loc_name_opt quotation_contents =
    <:str_item< let _ = $expand_vector_quot_expr _loc _loc_name_opt quotation_contents$ >>

   let _ =
    Syntax.Quotation.add "local" Syntax.Quotation.DynAst.expr_tag expand_vector_quot_expr;
    Syntax.Quotation.add "local" Syntax.Quotation.DynAst.str_item_tag expand_vector_quot_str_item;
    Syntax.Quotation.default := "local"

end

module Id = struct
  let name = "bsml_syntax"
  let version = "1.0"
end

let module M = Register.OCamlSyntaxExtension(Id) in
let module M' = M(Make) in ()

