open Bsml
open Stdlib
open Base
open Comm
open Tools

type mode = Max | Average

let mega = 1000000.

(* (unit -> unit) -> unit par *)
let doat0 f = mkpar(function 0 -> f() | _ -> ())

(* int list -> float list -> float * float *)
let leastsquares h t =
  let lenh = List.length h 
  and sumt = List.fold_left (+.) 0. t 
  and sumth = List.fold_left2 (fun s h t ->s+.((float)h*.t)) 0. h t
  and sumh = List.fold_left (+) 0 h
  and sumhh = List.fold_left (fun s h->s+h*h) 0 h in
    if lenh>sumh 
    then 
      let a = (float) sumh/.(float) lenh in
      let g =  (sumth-.a*.sumt)/.((float)sumhh-.a*.(float)sumh) in
      let l = (sumt-.(float)sumh*.g)/.((float) lenh) in g, l
    else
      let a = (float)lenh/.(float) sumh in
      let g = (sumt-.a*.sumth)/.((float)sumh-.a*.(float)sumhh) in
      let l = (sumth-.(float)sumhh*.g)/.(float) sumh in g, l

(* ('a -> 'a -> 'a) -> ('a -> 'b) -> 'a par -> 'b *)
let process_timing op1 op2 times =
  let ptime' = fold_direct op1 times in
  let ptime = parfun op2 ptime' in
    proj ptime 0 

(* 'a par -> 'a *)
let mintime times = process_timing min (fun s->s) times

(* 'a par -> 'a *)
let maxtime times = process_timing max (fun s->s) times

(* float par -> float *)
let avgtime times = 
  let nb = proj (fold_direct (+) (parfun (fun f->if f=0. then 0 else 1) times)) 0 in
  let op1 = (+.) 
  and op2 s = if nb=0 then 0. else s/.((float)nb) in
    process_timing op1 op2 times

(* int -> int -> float -> float *)
let toMflops niters n time =
  let nflops =(float) (4*niters*n) in
    if time>0. then nflops/.time else 0.

(* int -> float *)
let foi x = (float) x

(* int -> int -> float par *)
let determine_one_r niters n = 
  let a = 1./.3. and b = 4./.9.
  and z = Array.init n foi 
  and y = Array.init n foi 
  and x = Array.init n foi in
    begin
      start_timing();
      for iter=1 to niters do
	for i=0 to n-1 do
	  y.(i)<- a*.x.(i) +. y.(i);
	done;
	for i=0 to n-1 do
	  z.(i)<-z.(i)-.b*.x.(i);
	done;
      done;
      stop_timing();
      get_cost();
    end

(* int -> int -> mode -> float *)
let determine_r niters maxn mode =
  let rec mklist n1 n2 = if n1>n2 then [] else n1::(mklist (2*n1) n2) in 
  let ns = mklist 16 maxn in
  let mflops =  
    let f n = parfun (toMflops niters n) (determine_one_r niters n) in
      List.map f ns in
  let rs = List.map avgtime mflops in
    match mode with
      | Average -> (List.fold_left (+.) 0. rs)/.(float)(List.length ns)
      | Max ->     (List.fold_left max 0. rs)

(* int -> int -> float Bsml.par *)
let determine_one_g_and_l niters h =
  let rest = h mod (bsp_p-1) in
  let size1 = h/(bsp_p-1) in
  let size2 = if rest=0 then size1 else size1+1 
  and create size = if size>0 then Some(Array.make size foi) else None in
  let v1 = create size1 and v2 = create size2 in
  let msg pid dst = 
    let d = if pid<rest then 1 else 0 in
      if pid=dst 
      then None 
      else if dst<rest+d then v2 else v1 in
    begin
      start_timing();
      for iter=1 to niters do
	ignore (put(mkpar msg))
      done;
      stop_timing();
      get_cost()
    end

(* int -> int -> float -> float -> float * float * float * float *)
let determine_g_and_l niters maxh r r'= 
  let rec mklist n1 n2 = if n1>n2 then [] else n1::(mklist (n1+1) n2) in 
  let hs = mklist (bsp_p) maxh in 
  let timings = List.map (compose avgtime (determine_one_g_and_l niters)) hs in
  let g,l =  leastsquares hs timings in
    (g/.(float) niters)*.r,
    (l/.(float) niters)*.r,
    (g/.(float) niters)*.r',
    (l/.(float) niters)*.r'

let _ = 
  ignore (parprint print_string (mkpar(fun i->Sys.os_type)));
  let len = Array.length argv in
  let niters = if len<2 then 50    else  int_of_string (argv.(1))
  and maxn   = if len<3 then 512   else  int_of_string (argv.(2)) 
  and maxh   = if len<4 then 128   else  int_of_string (argv.(3))
  and verb   = if len<5 then false else bool_of_string (argv.(4)) in
    begin
      ignore (doat0(fun () -> 
	      print_string (if verb then "Verbose on\n" else "");
	      flush stdout;
	      if verb then
		begin
		  print_string "NITERS = ";print_int niters;print_newline();flush stdout;
		  print_string "MAXN   = ";print_int maxn;print_newline();flush stdout;
		  print_string "MAXH   = ";print_int maxh;print_newline();flush stdout;
		  print_newline();
		end));
      let sync = put << fun dst -> None >> in 
	doat0(fun () -> print_string "Benchmark starts\n");
	let r = determine_r niters maxn Average in
	let r' = determine_r niters maxn Max in
	let g,l,g',l' = determine_g_and_l niters maxh r r' in
	  doat0(fun () ->
		  print_string "p = ";print_int (bsp_p);print_newline();
		  print_string "r = ";print_float (r/.mega);
		  print_string " Mflops/s\n";
		  print_string "g = ";print_float g;print_string " flops/word\n";
		  print_string "l = ";print_float l;print_string " flops\n";
		  print_string "r' = ";print_float (r'/.mega);
		  print_string " Mflops/s\n";
		  print_string "g' = ";print_float g';print_string " flops/word\n";
		  print_string "l' = ";print_float l';print_string " flops\n"
	       )
      end
