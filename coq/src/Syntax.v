Require Import Environment.
Require Import EquivDec.
Require Import List.

Module Type EqType.

  Parameter t : Set.
  Declare Instance tt : EqDec t (@eq t).

End EqType.

(** * Semantics *)

Module Semantics (Var Loc Name : EqType) (Import E : Environment).

  (** ** Dynamic expressions *)
  
  (** Dynamic constants are static constants augmented with the constant [Critical']
      and the constant scheme [Ref], [Tid] and [MSetPartial] *)

  (** - [Critical'] denotes a running critical section *)
  (** - [Ref l] denotes a memory location *)
  (** - [Tid d] denotes a domain name *)
  (** - [MSetPartial l] denotes the partial application of the constant [MSet] *)

  (** The set of expression is simply updated to account for this change *)

  Inductive constant : Set :=
    Rec : constant
  | MRef : constant
  | MGet : constant
  | MSet : constant
  | Spawn : constant
  | Join : constant
  | Critical : constant
  | Critical' : constant
  | Wait : constant
  | Notify : constant
  | MSetPartial : Loc.t -> constant 
  | Ref : Loc.t -> constant
  | Tid : Name.t -> constant
  | Unit : constant.

  Inductive expression : Set :=
    Val : value -> expression
  | Var : Var.t -> expression
  | App : expression -> expression -> expression
  with value : Set :=
         Const : constant -> value
       | Lambda : Var.t -> expression -> value.

  Coercion Tid : Name.t >-> constant.
  Coercion Ref : Loc.t >-> constant.
  Coercion Val : value >-> expression.
  Coercion Const : constant >-> value.

  (** ** Contexts *)
  (** As usual, context are expressions with a hole enforcing the evaluation order.
      Expressions are evaluated from left to right and [fill C e] denotes the 
      context C filled with expression e. *)

  Inductive context : Set :=
    TopLevel : context
  | Left : context -> expression -> context
  | Right: value -> context -> context.
  
  Fixpoint fill (C : context) (e : expression) : expression :=
    match C with
      TopLevel => e
    | Left C' e' => App (fill C' e) e'
    | Right v C' => App v (fill C' e)
    end.

  (** ** Substitution *)
  
  Fixpoint sub (e0 : expression) (x : Var.t) (e : expression) : expression :=
    match e with
      Val (Const c) => Val (Const c)
    | Val (Lambda y e) =>
      if x ==b y then Val (Lambda y e) else Val (Lambda y (sub e0 x e))
    | Var y => if x ==b y then e else Var y
    | App e1 e2 => App (sub e0 x e1) (sub e0 x e2)
    end.

  (** ** Program state *)
  
  Inductive status : Set := NS | CS.
  
  Definition thread : Set := expression * status.

  Definition heap := E.t Loc.t value.
  
  Definition pool := E.t Name.t thread.

  Definition terminated (p : pool) (t : Name.t) (v : value) : Prop :=
    exists s, get p t = Some (v:expression, s). 

  Definition watching (p : pool) (t : Name.t) : Prop :=
    exists C e, get p t = Some (fill C (App Critical' e), CS).

  Definition notified (p : pool) (t : Name.t) : Prop :=
    exists t' C s, get p t' = Some (fill C (App Notify t), s).

  (** ** Semantics of constants **)

  Inductive call (t : Name.t) :
    constant -> value -> (status * heap * pool) ->
    expression -> (status * heap * pool) -> Prop :=
    call_fix : forall (x : Var.t) (e : expression) (s : status)
                      (sigma : heap) (T : pool),
      call t
           Rec (Lambda x e) (s, sigma, T)
           (sub (App Rec (Lambda x e)) x e) (s, sigma, T)
  | call_ref : forall (l : Loc.t) (v : value) (s : status) (sigma : heap) (T : pool),
      ~ In l (dom sigma) ->
      call t
           MRef v (s, sigma, T)
           l (s, sigma [l <- v], T)
  | call_get : forall (l : Loc.t) (v : value) (s : status) (sigma : heap) (T : pool),
      get sigma l = Some v ->
      call t
           MGet l (s, sigma, T)
           v (s, sigma, T)
  | call_set1 : forall (l : Loc.t) (v:value) s (sigma : heap) (T:pool),
      get sigma l = Some v ->
      call t
           MGet l (s, sigma, T)
           (MSetPartial l) (s, sigma, T)
  | call_set2 : forall (l:Loc.t) (v:value)(s:status)(sigma:heap)(T:pool),
      call t
           (MSetPartial l) v (s, sigma, T)
           Unit (s, sigma [l <- v], T)
  | call_spawn :
      forall (x : Var.t) (e : expression) (s:status) (sigma:heap)
             (T T' : pool) (d : Name.t),
        ~ In d (dom T) ->
        call t
             Spawn (Lambda x e) (s, sigma, T)
             d (s, sigma, T[t <- (e,s)])
  | call_join : forall (t' : Name.t) (v : value) (s : status) (sigma : heap) (T:pool),
      terminated T t' v ->
      call t
           Join t' (s, sigma, T)
           v (s, sigma, T)
  | call_enter1 : forall (v : value) (s : status) (sigma:heap) (T:pool),
      call t
           Critical v (NS, sigma, T)
           (App Critical' (App v Unit)) (s, sigma, T)
  | call_enter2 : forall (v : value) (s:status) (sigma : heap)(T:pool),
      call t
           Critical v (CS, sigma, T)
           (App v Unit) (s, sigma, T)
  | call_exit : forall (v : value) (sigma : heap) (T:pool),
      call t
           Critical' v (CS, sigma, T)
           v (NS, sigma, T)
  | call_wait : forall (sigma:heap) (T:pool),
      watching T t -> notified T t ->
      call t
           Wait Unit (CS, sigma, T)
           Unit (CS, sigma, T)
  | call_notify : forall (t' : Name.t) (s : status) (sigma:heap) (T:pool),
      ~ watching T t' ->
      call t
           Notify t' (s, sigma, T)
           Unit (s, sigma, T).

  (** ** Reduction rule *)
  
  Inductive step (t : Name.t) :
    thread * heap * pool -> thread * heap * pool -> Prop :=
    step_app : forall (x : Var.t) (e0:expression) (v : value) (s:status)
                      (sigma:heap) (T:pool),
      step t ((App (Lambda x e0) v, s), sigma, T) ((sub v x e0, s), sigma, T)
  | step_call :
      forall (v:value) (e e':expression) (c: constant) (s s':status)
             (sigma sigma':heap) (T T':pool),
        call t c v (s, sigma, T) e' (s', sigma', T') ->
        step t ((App c e, s),sigma, T) ((e', s'),sigma', T'). 
  
  Definition reduce (T1 T2 : pool) (sigma1 sigma2 : heap) : Prop :=
    exists t C e1 e2 s1 s2 T0,
      get T1 t = Some (fill C e1, s1) /\
      step t (e1,s1,sigma1,T1) (e2,s2,sigma2,T0) /\
      update T0 t (fill C e2,s2) = T2.
  
End Semantics.













