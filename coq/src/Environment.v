Require Import EquivDec.

Module Type Environment.
  
  Parameter t : Set -> Set -> Set.

  Section A.
    
    Variable A : Set.
    
    Variable B : Set.
    
    Declare Instance tt : EqDec A (@eq A).
    
    Parameter empty : t A B.
    
    Parameter get : t A B -> A -> option B.
    
    Parameter singleton : A -> B -> t A B.
    
    Parameter dom : t A B -> list A.

    Parameter update : t A B -> A -> B -> t A B.

  End A.

  Arguments get [A B] _ _.
  Arguments update [A B] _ _.
  Arguments dom [A B] _.

  Notation "E '[' x '<-' v ']'" := (update E x v) 
                                     (at level 65, left associativity).
  
End Environment.