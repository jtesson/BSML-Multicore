\documentclass[12pt]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{stmaryrd}
\usepackage{MnSymbol}
\title{Draft Semantics of OCaml Multicore Domains}
\date{}
\begin{document}
\maketitle
\newcommand{\alloc}{{\tt{ref}}}
\newcommand{\get}{{\tt{get}}}
\newcommand{\set}{{\tt{set}}}
\newcommand{\spawn}{{\tt{spawn}}}
\newcommand{\join}{{\tt{join}}}
\newcommand{\critical}{{\tt{critical}}}
\newcommand{\wait}{{\tt{wait}}}
\newcommand{\notify}{{\tt{notify}}}
\newcommand{\suspended}{{\mathit{suspended}}}
\newcommand{\terminated}{{\mathit{terminated}}}
\newcommand{\notified}{{\mathit{notified}}}
\newcommand{\watching}{{\mathit{watching}}}
\newcommand{\domain}{{\mathit{domain}}}
\newcommand{\reference}{{\mathit{ref}}}
\newcommand{\dom}{{\mathit{dom}}}
\newcommand{\unit}{{\mathit{unit}}}
\newcommand{\fix}{{\tt{fix}}}

\newcommand{\constant}{{\mathit{constant}}}
\newcommand{\val}{{\mathit{value}}}
\newcommand{\heap}{{\mathit{heap}}}
\newcommand{\pool}{{\mathit{pool}}}
\newcommand{\expression}{{\mathit{expression}}}
\newcommand{\name}{{\mathit{domain}}}
\newcommand{\location}{{\mathit{location}}}
\newcommand{\csem}[2]{\llparenthesis #1\rrparenthesis_{#2}}

\section{Syntax}
We consider a polymorphic $\lambda$-calculus extended with the following
constants
$$
\begin{array}{cc}
  \begin{array}{lcl}
    \fix &::& \forall \alpha.(\alpha \rightarrow \alpha) \rightarrow \alpha\\
    () &::& \unit\\
    \alloc &::& \forall \alpha.\alpha \rightarrow \alpha~\reference\\
    \get &::& \forall \alpha.\alpha~\reference \rightarrow \alpha\\
    \set &::& \forall \alpha.\alpha~\reference \rightarrow \alpha \rightarrow \unit
  \end{array}
  &
  \begin{array}{lcl}
    \spawn &::& \forall \alpha.(\unit \rightarrow \alpha) \rightarrow \alpha~\domain\\
    \join &::&  \forall \alpha.\alpha~\domain \rightarrow \alpha\\
    \critical &::& \forall \alpha.(\unit \rightarrow \alpha) \rightarrow \alpha\\
    \notify &::& \forall \alpha.\alpha~{\mathit{\domain}} \rightarrow \unit\\
    \wait &::& \unit \rightarrow \unit
  \end{array}
\end{array}
$$
where types are defines as follows:
$$
\begin{array}{lcll}
  \tau &::=& \alpha \mid \unit \mid \name \mid \tau~ref \mid \tau \rightarrow \tau \mid \forall \alpha.\tau
             & {\text{(types)}}
\end{array}
$$
Expressions are defined as follows:
$$
\begin{array}{lcll}
  c &::=& \alloc \mid \get \mid \set \mid \spawn \mid \join \mid \critical \mid \wait \mid \notify
  & {\text{(constants)}}\\
  e &::=& c \mid x \mid \lambda x . e \mid e~ e& {\text{(expressions)}}
\end{array}
$$

% $$
% \begin{array}{c}
%   \cfrac{c :: \tau}{\Gamma \vdash c:\tau}
%   \qquad
%   \cfrac{}{\Gamma,x :\tau \vdash x :\tau}
%   \qquad
%   \cfrac{\Gamma \vdash e: \forall \alpha.\tau}
%   {\Gamma \vdash e : \tau[\tau'/\alpha]}
%   \\\\
%   \cfrac{\Gamma, x:\tau_1 \vdash e:\tau_2}{\Gamma \vdash \lambda x.e : \tau_1 \rightarrow \tau_2}
%   \qquad
%   \cfrac{
%   \Gamma \vdash e_1 : \tau_1 \rightarrow \tau_2 \qquad \Gamma \vdash e_2 : \tau_1 
%   }{
%   \Gamma \vdash e_1~e_2 : \tau_2
%   }
% \end{array}
% $$

\section{Dynamic semantics}

We extend constants with memory locations $\location$,
domain names $\domain$ and special constants
$\set~\ell$ and $\overline{\critical}:\forall \alpha.\alpha \rightarrow \alpha$.
Intuitively, $\set~\ell$ is the result of the partial application of $\set$ to $\ell$
The type of locations, domain names and of the partial application $\set~\ell$ is
run-time dependent.
The function $\overline{\critical} :: \forall \alpha.\alpha \rightarrow \alpha$
closes a critical section and returns its parameter.
$$
\begin{array}{lclcll}
  constant &\owns& c &::=& \ldots \mid \ell \mid \set~\ell \mid t \mid\overline{\critical}
  \ell \in \location, t \in \domain\\
  value &\owns&v &::=& c \mid \lambda x .e\\
  context &\owns&C &::=& [] \mid C~e \mid v~C 
\end{array}
$$
The semantics is defined as a reduction relation. A state if a pair $(T,\sigma)$ where
$$
\begin{array}{lcll}
  T &:& \name \rightharpoonup \expression & {\text{(pool)}}\\
  \sigma &:& \location \rightharpoonup \val & {\text{(heap)}}
\end{array}
$$
Given a function $F:A \rightarrow B$, and elements $x \in A$ and $e \in B$,
we note $F[x \mapsto v]$ the function defined by
$$
\sigma[\ell \mapsto v] \triangleeq
(\sigma \setminus \{(\ell,v') \mid v' \in V\}) \uplus \{(\ell,v)\}
$$
To define the reduction rules we need to define some properties over thread pools.
\begin{itemize}
\item $\watching(T,t)$ holds if domain $t$ is in a critical section
  $$\watching(T,t) \triangleeq \exists C,e.T(t) = C[\overline{\critical}~e]$$
\item $\notified(T,t)$ holds if domain $t$ is notified
  $$\notified(T,t) \triangleeq \exists C,t'.T(t') = C[\notify~t]$$
\item $\suspended(T,t)$ holds if domain $t$ is suspended on a call to $\wait$
  $$\suspended(T,t) \triangleeq \watching(T,t) \wedge \exists C.T(t) = C[\wait~()]$$
\item $\terminated(T,t,v)$ states that domain $t$ has terminated with value $v$
  $$\terminated(T,t,v) \triangleeq T(t) = v$$
\end{itemize}

\noindent For all constant $c$ and domain $t$, we define the partial function
$$
\csem{c}{t} : (\val \times \heap \times \pool)
\rightarrow (\expression \times \heap \times \pool)
$$
Intuivitly $\csem{c}{t}(v,\sigma,T)$ denotes a call to $c$ applied to $v$ in context formed
by heap $\sigma$ and pool $T$. It produces a triple $(e,\sigma',T')$ resulting from
this call where $e$ is the result of the call and $\sigma'$ and $T'$ are the new heap and pool produces by this call.
$$
\begin{array}{c}
  \begin{array}{lcll}
    \csem{\alloc}{t}(v,\sigma,T) &=& (\ell,\sigma[\ell \mapsto v]) & {\text{if }}\ell \not \in \dom(\sigma)
    \\
    \csem{\get}{t}(\ell,\sigma,T) &=& (v,\sigma,T)&{\text{if }}\sigma(\ell) = v
    \\
    \csem{\set~\ell}{t}(v,\sigma,T)&=& ((),\sigma[\ell \mapsto v],T) &{\text{if }}\ell \in \dom(\sigma)
    \\                                                                                      
    \csem{\spawn}{t}(v,\sigma,T) &=& (t', \sigma, T[d \mapsto v~()])& t' \not \in \dom(T)
    \\
    \csem{\join}{t}(t', \sigma, T) &=&(v,s,\sigma) & {\text{if }} {\mathit{terminated}}(T,t,v)
    \\
    \csem{\critical}{t}(v,\circ,\sigma,T) &=& (\overline{\critical}~(v~()),\sigma,T)& {\text{if }} \neg \watching(T,t)
    \\
    \csem{\critical}{t}(v,\sigma,T) &=& (v~(),\sigma,T)& {\text{if }} \watching(T,t)
    \\
    \csem{\overline{\critical}}{t}(v,\sigma,T) &=& (v,\sigma,T)&
    \\
    \csem{\wait}{t}((),\sigma,T) &=& ((),\sigma,T)&{\text{if }}
                                                    {\mathit{\watching}}(T,t) {\text{ and }} {\mathit{\notified}}(T,t)
    \\
    \csem{\notify}{t}(t',\sigma,T) &=& ((), \sigma,T)&{\text{if }} \neg({\mathit{\watching}}(T,t'))
  \end{array}
\end{array}
$$
Finally the reduction relation is defined by rule $(red)$ given below. It relies on the reduction of redex as defined by rules ($\beta$-reduction) and (external).
$$
\begin{array}{cl}
  ((\lambda x.e)~v, \sigma, T) \rightarrow_t ((e[v/x],s), \sigma, T)
  & {\text{($\beta$-reduction)}}
  \\\\
  \cfrac{
  \llparenthesis c \rrparenthesis_t (v, \sigma,T) = (e,\sigma',T')
  }
  {
  (c~v, \sigma,T) \rightarrow_t (e,\sigma',T')
  }
  & {\text{(external)}}
  \\\\
  \cfrac{
  \begin{array}{c}
    T(t) = C[e] \qquad
    (e, \sigma,T) \rightarrow_t (e',\sigma',T') \\
  \end{array}
  }{
  (T,\sigma) \rightarrow (T'[t \mapsto C[e']] ,\sigma')
  }
  & {\text{(reduction)}}
\end{array}    
$$

\end{document}